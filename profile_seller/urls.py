from django.urls import path,include,re_path
from django.contrib.auth import views as auth_views
from . import views
from products.views import ProductDetails
from django.conf.urls import url


app_name='profile_seller'

urlpatterns=[
   path('',views.seller_page.as_view(),name='seller_page'),
   re_path(r'^(?P<slug>[\w-]+)/$',ProductDetails.as_view(),name='product_details'),

]

