from django.views.generic import TemplateView,ListView
from django.shortcuts import render
from accounts.models import Profile,Seller_Profile
from products.models import Product
from carts.models import Cart
from dikko.models import Dikko
from django.contrib.auth import get_user_model
User = get_user_model()

class seller_page(ListView):
    model = Seller_Profile
    template_name = 'profile_seller/profile.html'

    def get_queryset(self,*args,**kwargs):
        request=self.request
        dikko=Dikko.objects.get(url=self.kwargs['dikkourl'])
        seller = dikko.owner
        print(seller)
        profile= Profile.objects.filter(seller_profile=seller).first()
        pro=dikko.products.all()
        print(pro)
        return pro.all()
    def get_context_data(self, *, object_list=None, **kwargs):
        request = self.request
        print(self.kwargs['dikkourl'])
        context = super().get_context_data(**kwargs)
        dikko = Dikko.objects.get(url=self.kwargs['dikkourl'])
        seller = dikko.owner
        cart_obj, new_obj = Cart.objects.new_or_get(self.request)
        profile=Profile.objects.filter(seller_profile=seller).first()
        print(profile.user.first_name)
        products = dikko.products.all()

        context['dikko']=dikko
        context['products']=products
        context['profile']=profile
        context['cart']=cart_obj
        return context

