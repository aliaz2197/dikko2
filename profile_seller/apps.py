from django.apps import AppConfig


class ProfileSellerConfig(AppConfig):
    name = 'profile_seller'
