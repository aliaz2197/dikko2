from django.db import models
from django.utils import timezone

from dikko2.utils import  unique_complain_id_generator
from django.db.models.signals import pre_save,post_save
# Create your models here.
STATUS= (
    ('C', 'بررسی شده'), # Checked
    ('W', 'در نوبت بررسی'),  # Waiting
)
class Complaint(models.Model):
    complain_id = models.CharField(max_length=120,blank=True)
    user_email=models.EmailField()
    user_name=models.CharField(max_length=60,blank=True)
    user_phoneNumber=models.CharField(max_length=10)
    topic = models.CharField(max_length=60)
    created_date=models.DateTimeField()
    text=models.TextField()
    status=models.CharField(max_length=5,choices=STATUS)
    def __str__(self):
        return self.complain_id
    def save(self, *args, **kwargs):
        ''' On save, update timestamps '''
        if not self.id:
            self.created_date=timezone.localtime(timezone.now())
        return super(Complaint, self).save(*args, **kwargs)

def pre_save_create_order_id(sender,instance,*args,**kwargs):
    if not instance.complain_id:

        instance.complain_id=unique_complain_id_generator(instance)

pre_save.connect(pre_save_create_order_id,sender=Complaint)