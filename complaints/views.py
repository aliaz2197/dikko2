from django.shortcuts import render
from .forms import complainForm
from accounts.models import Profile
from django.contrib.auth.decorators import login_required
from .models import Complaint
# Create your views here.
def home(request):
    form=complainForm(request.POST or None)
    if request.POST:
        if form.is_valid():
            comp=form.save()
            comp.status='W'
            comp.save()
            tracking_code=comp.complain_id
            return render(request, 'complaints/submitted.html', {'trackingCode':tracking_code})


    return render(request,'complaints/complaints.html',{'form':form})
@login_required
def complainList(request):
    if request.user.admin:
        profile=Profile.objects.get(user=request.user)
        complains=Complaint.objects.all().order_by('-created_date')
        return render(request,'dashboard-complains-list.html',{'profile':profile,'complains':complains})