from django import forms
from .models import Complaint
class complainForm(forms.ModelForm):
    class Meta:
        model=Complaint
        fields=('user_email','user_name','text','topic','user_phoneNumber')
        widgets={
            'user_email':forms.EmailInput(attrs={'class':'form-control','placeholder':'ایمیل شما'}),
            'user_name': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'نام شما'}),
            'text': forms.Textarea(attrs={'class': 'form-control', 'placeholder': 'متن پیام'}),
            'user_phoneNumber': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'تلفن تماس شما'}),
            'topic': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'موضوع شکایت'}),
        }
        error_messages={
            'user_email':{
                'invalid':'لطفا ایمیل خود را وارد کنید',
            },

            'user_name':{
                'invalid':'لطفا نام خود را وارد کنید',
            },

            'text':{
                'invalid':'لطفا متن پیام خود را بنویسید',
            },
            'user_phoneNumber':{
                'invalid':'لطفا شماره تماس خود را وارد کنید',
            },
            'topic':{
                'invalid':'لطفا موضوع شکایت خود را وارد کنید',
            },
        }