from django.urls import path, include, re_path
from . import views

app_name="dikko"
urlpatterns=[
    re_path('product_list/(?P<sfield>[\w-]+)/(?P<slug>[\w-]+)/$', views.Product_List.as_view(), name='product_list'),
    re_path('dikko_list/(?P<slug>[\w-]+)/dikko_list/$', views.Dikko_List.as_view(), name='handcrafts_dikko_list'),

    # re_path('handcrafts/(?P<name>\w+)/',views.Product_List.as_view(),name='dikko_product_list'),
    re_path('(?P<pk>\w+)/$',views.SubField_List_Handcrafts.as_view(),name='subfield_list'),


]