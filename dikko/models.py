import sys
from django.db import models
from django.urls import reverse
from multiselectfield import MultiSelectField
from accounts.models import Seller_Profile
from products.models import Product
from dikko2.utils import unique_slug_generator
from django.db.models.signals import pre_save
from PIL import Image
from io import BytesIO
from django.core.files.uploadedfile import InMemoryUploadedFile
# Create your models here.
def get_upload_path(instance, filename):
    return 'dikko/img/{0}/{1}/{2}'.format(instance.owner.user.email,instance.name, filename)


MY_CHOICES = (('صنایع دستی', 'صنایع دستی'),
                  ('محصولات کشاورزی', 'محصولات کشاورزی'),
                  ('محصولات فرهنگی', 'محصولات فرهنگی'),
                  ('پوشاک', 'پوشاک'),
                  ('محصولات غذایی', 'محصولات غذایی'),
                  ('عطاری', 'عطاری')
                  )

def get_upload_path_Field(instance, filename):
    return 'fields/img/{0}/{1}'.format(instance.name, filename)
class Field(models.Model):
    name = models.CharField(max_length=30)
    image = models.ImageField(blank=True, null=True, upload_to=get_upload_path_Field)
    image2=models.ImageField(blank=True,null=True,upload_to=get_upload_path_Field)
    def __str__(self):
        return self.name

    def save(self):
        im = Image.open(self.image)
        im2=Image.open(self.image2)
        output = BytesIO()
        output2=BytesIO()
        im = im.resize((679, 550))
        # im2=im2.resize((1920,330))
        if im.mode in ("RGBA", "P"):
            im = im.convert("RGB")
        if im2.mode in ("RGBA", "P"):
            im2=im2.convert("RGB")
        im.save(output, format='JPEG', quality=100)
        im2.save(output2, format='JPEG', quality=100)
        output.seek(0)
        output2.seek(0)
        self.image = InMemoryUploadedFile(output, 'ImageField', "%s.jpg" % self.image.name.split('.')[0], 'image/jpeg',
                                          sys.getsizeof(output), None)
        self.image2=InMemoryUploadedFile(output2, 'ImageField', "%s.jpg" % self.image2.name.split('.')[0], 'image2/jpeg',
                                        sys.getsizeof(output2), None)
        super(Field, self).save()


def get_upload_path_Sub_Field(instance, filename):
    return 'sub_fields/img/{0}/{1}'.format(instance.name, filename)
class Sub_Field(models.Model):
    name = models.CharField(max_length=30)
    image=models.ImageField(blank=True,null=True,upload_to=get_upload_path_Sub_Field)
    image2=models.ImageField(blank=True, null=True, upload_to=get_upload_path_Sub_Field)
    field = models.ForeignKey(Field, on_delete=models.CASCADE)
    description=models.CharField(max_length=200,null=True,blank=True)
    scode=models.CharField(max_length=4,blank=True,null=True)
    slug = models.CharField(max_length=50,blank=True)
    def __str__(self):
        return self.name

    def save(self):
        im = Image.open(self.image)
        im2=Image.open(self.image2)
        output2=BytesIO()
        output = BytesIO()
        im = im.resize((261, 206))
        if im.mode in ("RGBA", "P"):
            im = im.convert("RGB")
        if im2.mode in ("RGBA", "P"):
            im2 = im2.convert("RGB")
        im.save(output, format='JPEG', quality=100)
        im2.save(output2, format='JPEG', quality=100)
        output.seek(0)
        output2.seek(0)
        self.image = InMemoryUploadedFile(output, 'ImageField', "%s.jpg" % self.image.name.split('.')[0], 'image/jpeg',
                                          sys.getsizeof(output), None)
        self.image2=InMemoryUploadedFile(output2, 'ImageField', "%s.jpg" % self.image2.name.split('.')[0], 'image2/jpeg',
                                        sys.getsizeof(output2), None)
        super(Sub_Field, self).save()


class Dikko(models.Model):

    owner=models.ForeignKey(Seller_Profile,on_delete=models.CASCADE,null=True,blank=True)
    name=models.CharField(max_length=20,blank=False)
    url=models.CharField(max_length=20,blank=True)
    image = models.ImageField(upload_to=get_upload_path, blank=True, null=True)
    image2=models.ImageField(upload_to=get_upload_path, blank=True, null=True)
    slug = models.SlugField(blank=True, null=True)
    field=models.ManyToManyField(Field,blank=True)
    sfield=models.ManyToManyField(Sub_Field,blank=True)
    products=models.ManyToManyField(Product,blank=True)
    description=models.TextField(blank=True)
    def __str__(self):
        return self.name

    def save(self):
            im = Image.open(self.image)
            im2=Image.open(self.image2)
            output = BytesIO()
            output2=BytesIO()
            im = im.resize((261, 206))
            if im.mode in ("RGBA", "P"):
                im = im.convert("RGB")
            if im2.mode in ("RGBA", "P"):
                im2 = im2.convert("RGB")
            im.save(output, format='JPEG', quality=100)
            im2.save(output2, format='JPEG', quality=100)
            output.seek(0)
            output2.seek(0)
            self.image = InMemoryUploadedFile(output, 'ImageField', "%s.jpg" % self.image.name.split('.')[0],
                                              'image/jpeg',
                                              sys.getsizeof(output), None)
            self.image2=InMemoryUploadedFile(output2, 'ImageField', "%s.jpg" % self.image2.name.split('.')[0],
                                            'image2/jpeg',
                                            sys.getsizeof(output2), None)

            super(Dikko, self).save()

            # def get_absolute_url(self):
    #     return reverse(
    #         "profile_seller:seller_page",
    #         kwargs={
    #             "dikkourl": self.url,
    #         }
    #     )
# def save_image(instance):
#     image=instance.image
#     im = Image.open(image)
#     output = BytesIO()
#     im = im.resize((261, 206))
#     if im.mode in ("RGBA", "P"):
#         im = im.convert("RGB")
#     im.save(output, format='JPEG', quality=100)
#     output.seek(0)
#     fimage = InMemoryUploadedFile(output, 'ImageField', "%s.jpg" % image.name.split('.')[0],
#                                       'image/jpeg',
#                                       sys.getsizeof(output), None)
#     return fimage
def dikko_pre_receiver(sender,instance,*args,**kwargs):
    if not instance.slug:
        instance.slug=unique_slug_generator(instance)

    # if not instance.image:
    #     instance.image=save_image(instance)

pre_save.connect(dikko_pre_receiver,sender=Dikko)
pre_save.connect(dikko_pre_receiver,sender=Sub_Field)

