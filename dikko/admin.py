from django.contrib import admin
from .models import Dikko,Field,Sub_Field

# Register your models here.
admin.site.register(Dikko)
admin.site.register(Field)
admin.site.register(Sub_Field)