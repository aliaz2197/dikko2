from django.shortcuts import render
from django.views.generic import ListView
from .models import Dikko,Sub_Field,Field
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
# Create your views here.

class SubField_List_Handcrafts(ListView):
    template_name = 'dikko/subfield-list.html'
    def get_queryset(self):
        pk=self.kwargs.get('pk')
        if pk=='handcrafts':
            sfields=Sub_Field.objects.filter(field__name='صنایع دستی').all()

            return sfields
        elif pk=='dresses':
            sfields=Sub_Field.objects.filter(field__name='پوشاک').all()

            return sfields
        elif pk=='agriculture':
            sfields=Sub_Field.objects.filter(field__name='محصولات کشاورزی').all()

            return sfields


    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        pk = self.kwargs.get('pk')
        field=None
        if pk == 'handcrafts':
            field=Field.objects.get(name='صنایع دستی')
            # field='صنایع دستی'
        elif pk =='dresses':
            field=Field.objects.get(name='پوشاک')
            # field='پوشاک'
        elif pk =='agriculture':
            field=Field.objects.get(name='محصولات کشاورزی')
            # field='محصولات کشاورزی'
        context['field'] =field
        return context

class Dikko_List(ListView):
    template_name = 'dikko/dikko_list.html'
    paginate_by = 20
    def get_queryset(self):
        sfield=self.kwargs.get('slug')
        print(sfield)
        sfields=Dikko.objects.filter(sfield__slug=sfield).all()

        return sfields

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        slug = self.kwargs.get('slug')
        sfield = Sub_Field.objects.get(slug=slug)
        context['sfield']=sfield
        return context

class Product_List(ListView):
    template_name = 'dikko/product-list.html'
    paginate_by = 10
    def get_queryset(self):
        slug=self.kwargs.get('slug')
        print(slug)
        dikko=Dikko.objects.filter(slug=slug).first()
        products=[]
        for pro in dikko.products.all():
            if pro.is_active and pro.available_quantity>0:
                products.append(pro)
                print(pro.dikko_slug)
                print(pro.slug)
        return products

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        slug = self.kwargs.get('slug')
        dikko = Dikko.objects.filter(slug=slug).first()
        context['dikko'] = dikko
        return context