from django.apps import AppConfig


class DikkoConfig(AppConfig):
    name = 'dikko'
