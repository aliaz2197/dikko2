from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import JsonResponse
from django.shortcuts import render
from django.urls import reverse
from django.utils import timezone
from django.views.generic import CreateView

from accounts.models import Profile
from .models import Discount
from carts.models import Cart
from django.core import serializers
from .forms import Crete_Code_Form
# Create your views here.
def apply_discount(request):

    if request.is_ajax():
        code = request.GET.get('discount_code')
        discount=None
        try:

            discount = Discount.objects.get(code=code)
        except:
            final={'valid':0,"message":"کد تخفیف وارد شده معتبر نیست"}
            return JsonResponse(final,safe=False)

        users=discount.users.all()
        print(request.user)
        user_discounts=request.user.discount_set.all()

        for user_discount in user_discounts:
            if user_discount.status=='اضافه شده':
                    final = {"valid": 0, "message": "فقط از یک کد تخفیف می توانید استفاده کنید"}
                    return JsonResponse(final, safe=True)
            elif user_discount.name==discount.name:
                if user_discount.status=='مصرف شده':
                    final = {"valid": 0, "message": "شما از این کد تخفیف قبلا استفاده کرده اید"}
                    return JsonResponse(final, safe=True)

        if discount.expire_date<timezone.now():
            final = {"valid": 0, "message": "مدت اعتبار این کد تخفیف به اتمام رسیده است"}
            return JsonResponse(final, safe=True)

        discount.status = 'اضافه شده'
        discount.save()
        discount.users.add(request.user)
        fuck=request.user.discount_set.all()
        for kir in fuck:
            print(kir.status)
            print(kir.name)
        cart_id = request.GET.get('cart_id')
        cart_obj=Cart.objects.get(id=cart_id)
        cart_obj.has_discount=True
        cart_obj.save()
        percent=discount.percent
        car_total=cart_obj.total
        print(car_total)
        dis=percent*car_total/100
        cart_obj.old_total =car_total
        print("dis")
        print(dis)
        print("final")
        print(request.user)
        print("howw")
        print(car_total-dis)
        gain=car_total-dis
        # cart_obj.total=car_total-dis
        cart_obj.save()
        print(cart_obj.total)

        final={"total":"{{cart_obj.total|persian_int}}","gain":dis,"old_total":"{{cart_obj.old_total|persian_int}}"}
        return JsonResponse(final,safe=False)

    cart_id=request.POST.get('cart_id')
    # try:
    #     discount=Discount.objects.get(code=code)
    #     cart=Cart.objects.get(id=cart_id)
    #     print(cart)
    #     print(discount)
    #
    #
    # except:
    #     print("not valid code")
class Create_Code(LoginRequiredMixin,CreateView):
    template_name = 'dashboard-create-discountCode.html'
    model = Discount
    form_class =Crete_Code_Form
    def get_success_url(self):
        return reverse('dashboard:dashboard_discount_codes')

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        pro = Profile.objects.filter(user=self.request.user).first()
        context['profile'] = pro
        return context





