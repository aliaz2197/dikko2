# Generated by Django 2.0.2 on 2018-11-24 20:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('discount_codes', '0007_auto_20181124_1515'),
    ]

    operations = [
        migrations.AddField(
            model_name='discount',
            name='status',
            field=models.CharField(choices=[('ایجاد شده', 'ایجاد شده'), ('اضافه شده', 'اضافه شده'), ('مصرف شده', 'مصرف شده'), ('منقضی شده', 'منقضی شده')], default='ایجاد شده', max_length=120),
        ),
    ]
