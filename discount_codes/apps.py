from django.apps import AppConfig


class DiscountCodesConfig(AppConfig):
    name = 'discount_codes'
