from attr.filters import exclude
from django import forms
from .models import Discount

from jalali_date.fields import JalaliDateField, SplitJalaliDateTimeField
from jalali_date.widgets import AdminJalaliDateWidget, AdminSplitJalaliDateTime
class Crete_Code_Form(forms.ModelForm):

    class Meta:
        model=Discount
        fields=('name','code','partner','percent','upper_limit','down_limit','expire_date')

        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'}),
            'code': forms.TextInput(attrs={'class': 'form-control'}),
            'partner': forms.TextInput(attrs={'class': 'form-control'}),
            'percent': forms.TextInput(attrs={'class': 'form-control'}),
            'upper_limit': forms.TextInput(attrs={'class': 'form-control'}),
            'down_limit': forms.TextInput(attrs={'class': 'form-control'}),
            # 'expire_date': forms.DateTimeInput(attrs={'class': 'form-control'}),
            # 'description':forms.TextInput()
        }

    def __init__(self, *args, **kwargs):
            super(Crete_Code_Form, self).__init__(*args, **kwargs)
            self.fields['expire_date'] = JalaliDateField(label=('date'),
                                                  widget=AdminJalaliDateWidget  # optional, for user default datepicker
                                                  )
            self.fields['expire_date'].widget.attrs.update({'class': 'jalali_date-date'})