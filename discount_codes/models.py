from django.db import models
from accounts.models import User
DISCOUNT_STATUS_CHOICES=(
    ('ایجاد شده','ایجاد شده'),
    ('اضافه شده','اضافه شده'),# add to cart but not used
    ('مصرف شده','مصرف شده'),
    ('منقضی شده','منقضی شده'),

)
# Create your models here.
class Discount(models.Model):
    name=models.CharField(max_length=50,unique=True)
    code=models.CharField(max_length=15,unique=True)
    partner=models.CharField(max_length=50,blank=True)
    status = models.CharField(max_length=120, default='ایجاد شده', choices=DISCOUNT_STATUS_CHOICES)
    percent=models.PositiveIntegerField()
    upper_limit=models.PositiveIntegerField(blank=True,null=True)
    down_limit=models.PositiveIntegerField(blank=True,null=True)
    expire_date=models.DateTimeField()
    created_date=models.DateTimeField(auto_now_add=True)
    users=models.ManyToManyField(User,blank=True)
    def __str__(self):
        return self.name