from django.urls import path, include, re_path
from . import views

app_name="discount_codes"
urlpatterns=[
    path('',views.apply_discount,name='apply_discount'),
]