from django.contrib import admin
from .models import Profile,Seller_Profile,City,State,Address,Reciever
from django.contrib.auth.models import Group
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin


from .forms import UserAdminCreationForm, UserAdminChangeForm
from .models import User
# Register your models here.
admin.site.register(Address)
admin.site.register(City)
admin.site.register(State)
admin.site.register(Seller_Profile)
admin.site.register(Reciever)
class ProfileAdmin(admin.ModelAdmin):
    list_display = ('user','is_seller')
    search_fields = ('user',)
admin.site.register(Profile,ProfileAdmin)


from .models import User

class UserAdmin(BaseUserAdmin):
    # The forms to add and change user instances
    form = UserAdminChangeForm
    add_form = UserAdminCreationForm

    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference specific fields on auth.User.
    list_display = ('email','first_name','last_name', 'admin','is_staff')
    list_filter = ('admin',)
    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        ('Personal info', {'fields': ( 'email','first_name','last_name')}),
        ('Permissions', {'fields': ('is_active', 'admin',)}),
        ('Important dates',{'fields':('last_login','date_joined')}),
    )
    # add_fieldsets is not a standard ModelAdmin attribute. UserAdmin
    # overrides get_fieldsets to use this attribute when creating a user.
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2')}
        ),
    )
    search_fields = ('email',)
    ordering = ('email',)
    filter_horizontal = ()


admin.site.register(User, UserAdmin)



# Remove Group Model from admin. We're not using it.
admin.site.unregister(Group)