from django.contrib.auth.decorators import login_required
from django.core.checks import messages
from django.db import transaction
from django.views.generic import TemplateView
from django.shortcuts import render, redirect
from django.urls import reverse
from django.http import HttpResponseRedirect
from .forms import UserForm, SellerForm, SellerForm2, UserForm_pro,LoginForm
from . import models
# from django.contrib.auth.models import User
from .models import User, State,Profile
from django.contrib.auth import get_user_model
from django.contrib.auth import login, authenticate
from django.views.generic import CreateView
from django.urls import reverse_lazy
from django.contrib.auth.decorators import user_passes_test
from django.contrib.auth import REDIRECT_FIELD_NAME
from django.http import JsonResponse
from django.contrib.sites.shortcuts import get_current_site
from django.template.loader import render_to_string
from .tokens import account_activation_token
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.utils.encoding import force_bytes, force_text
from django.core.mail import send_mail
from django.utils.http import is_safe_url
from django.conf import settings


# Create your views here.
# class SignUp(CreateView):
#     form_class =UserForm
#     success_url = reverse_lazy("login")
#     template_name = "accounts/signup.html"
#

def validate_dikko_url(request):
    dikko_url=request.GET.get('dikko_url', None)
    data={
        'is_taken': models.Seller_Profile.objects.filter(dikko_url__iexact=dikko_url).exists()
    }
    return JsonResponse(data)


def check_seller_user(user):
    return models.Profile.objects.all().filter(user=user, is_seller=False)


def check_seller(function=None, redirect_field_name=REDIRECT_FIELD_NAME, login_url=None):
    actual_decorator=user_passes_test(
        check_seller_user,
        login_url=login_url,
        redirect_field_name=redirect_field_name
    )
    if function:
        return actual_decorator(function)
    return actual_decorator


@check_seller(login_url="/accounts/login")
def SignUpSeller(request):
    if request.method=='POST':
        seller_form=SellerForm(data=request.POST)
        print(seller_form.errors)
        if seller_form.is_valid():
            seller=models.Seller_Profile()
            seller.dikko_name=seller_form.cleaned_data.get('dikko_name')
            seller.user=request.user
            seller.my_field=seller_form.cleaned_data.get('my_field')
            seller.dikko_url=seller_form.cleaned_data.get('dikko_url')
            seller.dikko_address=seller_form.cleaned_data.get('dikko_address')
            seller.dikko_phone_number=seller_form.cleaned_data.get('dikko_phone_number')
            print(seller_form.cleaned_data.get('dikko_url'))
            pro=models.Profile.objects.get(user=request.user)
            pro.is_seller=True
            seller.save()
            pro.seller_profile=seller
            pro.save()
            print(seller.dikko_url)
            # url=reverse('products:seller_page')
            url=models.Seller_Profile.get_absolute_url(seller)
            # print(url)
            # return render(request,'products/profile.html',{'dikko_url':seller.dikko_url})
            return HttpResponseRedirect(url)
            # return redirect('products:seller_page',pk=seller.dikko_url)

            # return HttpResponseRedirect(reverse('accounts:signup_seller2'))
        else:
            return render(request, 'accounts/signup_seller.html', {'seller_form': seller_form})
    else:

        seller_form=SellerForm()
        return render(request, 'accounts/signup_seller.html', {'seller_form': seller_form})


# def SignUpSeller2(request):
#     if request.method=='POST':
#         seller_form=SellerForm2(data=request.POST)
#         print(seller_form.errors)
#         if seller_form.is_valid():
#             seller=models.Seller_Profile.objects.get(user=request.user)
#             seller.dikko_address=seller_form.cleaned_data.get('dikko_address')
#             seller.dikko_phone_number=seller_form.cleaned_data.get('dikko_phone_number')
#             seller.save()
#             url = models.Seller_Profile.get_absolute_url(seller)
#             return HttpResponseRedirect(url)
#     else:
#         seller_form=SellerForm2()
#         return render(request,'accounts/signup_seller2.html',{'seller_form':seller_form})

def SignUp(request):
    if request.method=='POST':
        user_form=UserForm(data=request.POST)
        user_form_pro=UserForm_pro(data=request.POST)
        print(user_form.errors)
        print(user_form_pro.errors)
        if user_form_pro.is_valid():

            if user_form.is_valid():

                # user=profile['user']
                # profile.save()

                username=user_form.cleaned_data.get('username')
                password=user_form.cleaned_data.get('password1')
                email=user_form.cleaned_data.get('email')

                u=User()
                print("hellooooooooo")

                u.set_password(request.POST['password1'])
                u.email=request.POST['email']
                u.first_name=request.POST['first_name']
                u.last_name=request.POST['last_name']
                # u.active=False
                u.is_active=False
                u.save()
                pro=models.Profile()
                # pro.birth_date=user_form.cleaned_data.get('birth_date')
                pro.user=u
                pro.save()
                current_site=get_current_site(request)
                subject='تایید حساب کاربری'
                message=render_to_string('accounts/account_activation_email.html', {
                    'user': u,
                    'domain': current_site.domain,
                    'uid': urlsafe_base64_encode(force_bytes(u.pk)).decode(),
                    'token': account_activation_token.make_token(u),
                })
                try:
                    send_mail(subject, message, 'بق بند <info@boghband.com>',
                          [email, ])
                    print("sent")
                except:
                    print("CAN NOTTTTTTT")
                return redirect('accounts:account_activation_sent',email=email)
            else:
                # user_form_pro=UserForm_pro()
                # user_form=UserForm()
                return render(request, 'accounts/signup.html', {'user_form': user_form, 'user_form_pro': user_form_pro})

        else:
            # user_form_pro=UserForm_pro()
            # user_form=UserForm()
            return render(request, 'accounts/signup.html', {'user_form': user_form, 'user_form_pro': user_form_pro})
            # u.email_user(subject, message)
            # pro=models.Profile()
            # pro.birth_date=user_form.cleaned_data.get('birth_date')
            # pro.user=u
            # pro.save()

            # user=authenticate(username=email, password=password)
            # print("ssdfffffffffffffffffff")
            # if user:
            #     # user.is_staff=False
            #     login(request, user)
            #     return HttpResponseRedirect(reverse('home'))
            # else:
            #     return render(request, 'accounts/signup.html', {'user_form': user_form})

    else:
        user_form_pro=UserForm_pro()
        user_form=UserForm()
        # profile_form=ProfileForm()
        # user_form = UserForm(instance=request.user)
        # profile_form = ProfileForm(instance=request.user.profile)
        return render(request, 'accounts/signup.html', {'user_form': user_form, 'user_form_pro': user_form_pro})


def account_activation_sent(request,email):
    user=User.objects.get(email=email)
    return render(request, 'accounts/account_activation_sent.html',{'name':user.first_name,'email':email})

def activate(request, uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except (TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None

    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        print("activvvvvvvvvvvvvvvvv")
        # user.profile.email_confirmed = True
        # profile=Profile.objects.get(user=user)
        # profile.email_confirmed=True
        # profile.save()
        # print(user.profile)
        # print(user.profile.email_confirmed)
        user.save()
        # login(request, user,backend='django.contrib.auth.backends.ModelBackend')
        return redirect('accounts:login',confirmed=token)
    else:
        return render(request, 'accounts/account_activation_invalid.html')

def loginView(request,confirmed=''):
    form=LoginForm(request.POST or None)
    print(form.errors)
    if request.POST and form.is_valid():
        email=form.cleaned_data.get('email')
        password=form.cleaned_data.get('password')

        user=authenticate(username=email, password=password)
        if user:
            login(request, user,backend='django.contrib.auth.backends.ModelBackend')
            try:
                if request.session['nextPage'] is not None:
                    next=request.session['nextPage']
                    request.session['nextPage']=None
                    return HttpResponseRedirect(next)
                else:
                    return HttpResponseRedirect(reverse('home'))
            except:
                return HttpResponseRedirect(reverse('home'))
    if request.GET.get('next'):
        if is_safe_url(url=request.GET.get('next'),allowed_hosts=settings.ALLOWED_HOSTS,require_https=request.is_secure()):
            request.session['nextPage']=request.GET.get('next')
    if confirmed:
        message='True'
    else:
        message='False'
    return render(request, 'registration/login.html', {'form': form,'message':message})