from django.core.files.uploadedfile import InMemoryUploadedFile
from django.db import models
from django.contrib.auth.models import AbstractUser
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils import timezone
from multiselectfield import MultiSelectField
 # from django.contrib.auth.models import User
from django.utils.functional import curry
from django.urls import reverse
from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser
)

from products.models import Product

class State(models.Model):
    name = models.CharField(max_length=30)

    def __str__(self):
        return self.name


class City(models.Model):
    state = models.ForeignKey(State, on_delete=models.CASCADE)
    name = models.CharField(max_length=30)

    def __str__(self):
        return self.name


class UserManager(BaseUserManager):
    def create_user(self, email, password=None):
        """
        Creates and saves a User with the given email and password.
        """
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(
            email=self.normalize_email(email),
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_staffuser(self, email, password):
        """
        Creates and saves a staff user with the given email and password.
        """
        user = self.create_user(
            email,
            password=password,
        )
        user.staff = True
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password):
        """
        Creates and saves a superuser with the given email and password.
        """
        user = self.create_user(
            email,
            password=password,
        )
        user.staff = True
        user.admin = True
        user.save(using=self._db)
        return user

class User(AbstractBaseUser):
    date_joined = models.DateTimeField(('date joined'), default=timezone.now)
    # last_visit = models.DateTimeField(('last visit'))
    email=models.EmailField(verbose_name='email address',max_length=255,unique=True)
    first_name = models.CharField(max_length=35)
    last_name = models.CharField(max_length=45)
    # pro=models.ForeignKey('Profile',on_delete=models.CASCADE,related_name='user_profile',null=True)
    # active = models.BooleanField(default=True)
    is_active=models.BooleanField(default=False)
    staff = models.BooleanField(default=False)  # a admin user; non super-user
    admin = models.BooleanField(default=False)  # a superuser
    # notice the absence of a "Password field", that's built in.
    objects = UserManager()
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []  # Email & Password are required by default.

    def get_full_name(self):
        # The user is identified by their email address
        full_name=self.first_name+" "+self.last_name
        return full_name
    def get_short_name(self):
        # The user is identified by their email address
        return self.first_name

    def __str__(self):  # __unicode__ on Python 2
        return self.email

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        # Simplest possible answer: Yes, always
        return True

    @property
    def is_staff(self):
        "Is the user a member of staff?"
        return self.staff

    @property
    def is_admin(self):
        "Is the user a admin member?"
        return self.admin

    # @property
    # def is_active(self):
    #     "Is the user active?"
    #     return self.active
class Reciever(models.Model):
    broadcast=models.BooleanField(default=False)


class Profile(models.Model):
    # first_name=models.CharField(max_length=25,blank=True)
    # last_name=models.CharField(max_length=35,blank=True)
    user=models.OneToOneField(User,on_delete=models.CASCADE)
    bio = models.TextField(max_length=500, blank=True)
    location = models.CharField(max_length=30, blank=True)
    birth_date = models.DateField(null=True, blank=True)
    is_seller=models.BooleanField(default=False)
    email_confirmed=models.BooleanField(default=False)
    # address=models.CharField(max_length=100,blank=True)
    # notification=models.ForeignKey(Notification,on_delete=models.SET_NULL,null=True,blank=True)
    state = models.CharField(max_length=30,blank=True,null=True)
    city = models.CharField(max_length=30,blank=True,null=True)
    phone_number=models.CharField(blank=True,max_length=10)
    national_code=models.CharField(max_length=10,blank=True)
    credit_card=models.CharField(max_length=16,blank=True)
    Reciever=models.ForeignKey(Reciever,on_delete=models.SET_NULL,blank=True,null=True)
    seller_profile=models.ForeignKey('Seller_Profile',models.SET_NULL,null=True,blank=True)


    def __str__(self):
        return self.user.email
class Address(models.Model):
    profile=models.ForeignKey(Profile,on_delete=models.CASCADE,null=True,blank=True)
    receiver_name=models.CharField(max_length=60,blank=False)
    receiver_phone_number=models.CharField(max_length=11,blank=False)
    state=models.CharField(max_length=30,blank=False)
    city=models.CharField(max_length=30,blank=False)
    postal_code=models.CharField(max_length=10)
    address=models.CharField(max_length=300)


def get_upload_path(instance, filename):
        return 'dikko/img/{0}/{1}'.format(instance.dikko_name, filename)
class Seller_Profile(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE,related_name='seller_profile')
    MY_CHOICES = (('صنایع دستی', 'صنایع دستی'),
                  ('محصولات کشاورزی', 'محصولات کشاورزی'),
                  ('محصولات فرهنگی', 'محصولات فرهنگی'),
                  ('پوشاک', 'پوشاک'),
                  ('محصولات غذایی', 'محصولات غذایی'),
                  ('عطاری', 'عطاری')
                  )

    # def get_absolute_url(self):
    #     return reverse(
    #         "profile_seller:seller_page",
    #         kwargs={
    #             "dikkourl": self.dikko_url,
    #         }
    #     )

    def __str__(self):
        return self.user.email


    # def save(self):
    #     im = Image.open(self.image)
    #     output = BytesIO()
    #     im = im.resize((679, 550))
    #     if im.mode in ("RGBA", "P"):
    #         im = im.convert("RGB")
    #     im.save(output, format='JPEG', quality=100)
    #     output.seek(0)
    #     self.image = InMemoryUploadedFile(output, 'ImageField', "%s.jpg" % self.image.name.split('.')[0], 'image/jpeg',
    #                                       sys.getsizeof(output), None)
    #     super(Seller_Profile, self).save()

