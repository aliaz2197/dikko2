# Generated by Django 2.0.2 on 2018-11-23 18:05

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0015_auto_20181123_2134'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='last_visit',
            field=models.DateTimeField(default=datetime.datetime(2018, 11, 23, 18, 5, 46, 337865, tzinfo=utc), verbose_name='last visit'),
        ),
    ]
