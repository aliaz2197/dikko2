from django.conf.urls import url
from django.urls import path, include, re_path
from django.contrib.auth import views as auth_views
from . import views

app_name='accounts'

urlpatterns=[
    path('signup/', views.SignUp, name="signup"),
    path('google/', include('allauth.urls')),
    path('login/', views.loginView, name="login"),
    re_path(r'^login/(?P<confirmed>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$', views.loginView, name="login"),
    path('logout/', auth_views.logout, name="logout"),
    path('signup_seller/', views.SignUpSeller, name="signup_seller"),
    # path('signup_seller2/', views.SignUpSeller2, name="signup_seller2"),
    path('validate_dikko_url/', views.validate_dikko_url, name='validate_dikko_url'),
    path('(?p<dikko_url>\w+)', include('profile_seller.urls'), name='seller_page'),
    url(r'^account_activation_sent/(?P<email>[\w.%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4})$',
        views.account_activation_sent, name='account_activation_sent'),
    url(r'^activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        views.activate, name='activate'),
    url(r'^password_reset/$', auth_views.password_reset, name='password_reset'),
    url(r'^password_reset/done/$', auth_views.password_reset_done, name='password_reset_done'),
    url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        auth_views.password_reset_confirm, name='password_reset_confirm'),
    url(r'^reset/done/$', auth_views.password_reset_complete, name='password_reset_complete'),
    url(r'^password/change/$',
        auth_views.PasswordChangeView.as_view(),
        name='password_change'),
    url(r'^password/change/done/$',
        auth_views.PasswordChangeDoneView.as_view(),
        name='password_change_done'),

]
