from django import forms
from django.contrib.auth import authenticate

from . import models
from django.db import transaction
from django.contrib.auth.forms import UserCreationForm
# from django.contrib.auth.models import User
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from .models import User
from django.core import validators
from django.core.exceptions import ValidationError
from multiselectfield import MultiSelectField
class UserForm_pro(forms.ModelForm):
    class Meta:
        model=models.User
        fields=('first_name','last_name')
        widgets={
            'first_name':forms.TextInput(attrs={'class':'form-control'}),
            'last_name': forms.TextInput(attrs={'class': 'form-control'}),
        }
class UserForm(forms.ModelForm):
    # username = forms.CharField(label='Enter Username', min_length=4, max_length=150)
    email = forms.EmailField(label='Enter email',widget=forms.EmailInput(attrs={'class':'form-control'}))
    password1 = forms.CharField(label='Enter password', widget=forms.PasswordInput(attrs={'class':'form-control'}))
    password2 = forms.CharField(label='Confirm password', widget=forms.PasswordInput(attrs={'class':'form-control'}))

    # birthdate=forms.DateField();
    # first_name=forms.CharField(max_length=25)
    # last_name=forms.CharField(max_length=35)
    CITY_1 = 'city_1'
    CITY_2 = 'city_2'
    CITY_CHOICES = (
        (CITY_1, u"City 1"),
        (CITY_2, u"City 2")
    )
    # cities = forms.MultipleChoiceField(choices=CITY_CHOICES)
    cities = MultiSelectField(choices=CITY_CHOICES)
    class Meta:
        model=models.Profile
        fields =( "email", "password1", "password2")
    # def clean_username(self):
    #     username = self.cleaned_data['username'].lower()
    #     r = User.objects.filter(username=username)
    #     if r.count():
    #         raise ValidationError("این نام کاربری قبلا وجود دارد")
    #     return username

    def clean_email(self):
        email = self.cleaned_data['email'].lower()
        r = User.objects.filter(email=email)
        if r.count():
            raise ValidationError("ایمیل وارد شده از قبل وجود دارد")
        return email

    def clean_password2(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')

        if password1 and password2 and password1 != password2:
            raise ValidationError("Password don't match")

        return password2




#
class SellerForm(forms.ModelForm):
    pass
#
#     class Meta:
#         model=models.Seller_Profile
#         fields = ("dikko_name","my_field","dikko_url","dikko_address","dikko_phone_number")
#         widgets = {
#             'dikko_address': forms.Textarea(attrs={'cols': 32, 'rows': 5}),
#             'dikko_name':forms.TextInput(attrs={'placeholder':'حاج علی'}),
#             'dikko_url':forms.TextInput(attrs={'placeholder':'alidikko'}),
#             'dikko_phone_number':forms.TextInput(attrs={'placeholder':'9378634654'})
#         }
#
#     def clean_dikko_name(self):
#         dikko_name=self.cleaned_data['dikko_name']
#         r=models.Seller_Profile.objects.filter(dikko_name=dikko_name)
#         if r.count():
#             raise ValidationError("نام دیکو از قبل وجود دارد")
#         return dikko_name
#
#     def clean_dikko_url(self):
#         dikko_url=self.cleaned_data['dikko_url'].lower()
#         print("clean")
#         print(dikko_url)
#         r=models.Seller_Profile.objects.filter(dikko_url=dikko_url)
#         print(r.count())
#         if r.count():
#             raise  ValidationError("این آدرس از قبل رزرو شده است")
#
#         return dikko_url
    # def __init__(self, *args, **kwargs):
    #     super(SellerForm, self).__init__(*args, **kwargs)
    #     self.fields['dikko_name'].help_text='sdfsdf'

class SellerForm2(forms.ModelForm):
    pass
#
#     class Meta:
#         model=models.Seller_Profile
#         fields = ("dikko_name","my_field","dikko_url","dikko_address","dikko_phone_number")
#
#
#     def clean_dikko_name(self):
#         dikko_name=self.cleaned_data['dikko_name']
#         r=models.Seller_Profile.objects.filter(dikko_name=dikko_name)
#         if r.count():
#             raise ValidationError("نام دیکو از قبل وجود دارد")
#         return dikko_name
#
#     def clean_dikko_url(self):
#         dikko_url=self.cleaned_data['dikko_url'].lower()
#         print("clean")
#         print(dikko_url)
#         r=models.Seller_Profile.objects.filter(dikko_url=dikko_url)
#         print(r.count())
#         if r.count():
#             raise  ValidationError("این آدرس از قبل رزرو شده است")
#
#         return dikko_url
#     # def __init__(self, *args, **kwargs):
#     #     super(SellerForm, self).__init__(*args, **kwargs)
#     #     self.fields['dikko_name'].help_text='sdfsdf'
#
#
class UserAdminCreationForm(forms.ModelForm):
    """A form for creating new users. Includes all the required
    fields, plus a repeated password."""

    password1 = forms.CharField(label='Password', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Password confirmation', widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ('email',)

    def clean_password2(self):
        # Check that the two password entries match
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Passwords don't match")
        return password2

    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super(UserAdminCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user


class UserAdminChangeForm(forms.ModelForm):
    """A form for updating users. Includes all the fields on
    the user, but replaces the password field with admin's
    password hash display field.
    """
    password = ReadOnlyPasswordHashField()

    class Meta:
        model = User
        fields = ('email','first_name','last_name', 'password', 'is_active', 'admin')

    def clean_password(self):
        # Regardless of what the user provides, return the initial value.
        # This is done here, rather than on the field, because the
        # field does not have access to the initial value
        return self.initial["password"]

class LoginForm(forms.Form):
    email = forms.CharField(max_length=255, required=True,widget=forms.EmailInput(attrs={'class':'form-control'}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={'class':'form-control'}), required=True,)

    def clean(self):
        email = self.cleaned_data.get('email')
        password = self.cleaned_data.get('password')

        try:
            user=authenticate(username=email, password=password)
            if not user.is_active:
                raise forms.ValidationError("متاسفیم! حساب کاربری فعال نشده است. لطفا به ایمیل خود مراجعه کنید")
        except:

            raise forms.ValidationError("متاسفیم! ایمیل یا کلمه عبور اشتباه وارد شده است")
        return self.cleaned_data

