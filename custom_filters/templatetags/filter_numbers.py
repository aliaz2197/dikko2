from django import template

register=template.Library()


@register.filter(name='persian_int')
def persian_int(english_int):
    devanagari_nums=('۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹', '.')

    number=str(english_int)
    f=number.split('.')
    # return ''.join(devanagari_nums[int(digit)] for digit in f[0])
    try:
        x=''.join(devanagari_nums[int(digit)] for digit in f[0])

        if f[1]=='00':
            s_split=[]
            count=0
            index=0
            for s in x:
                if (count+1) % 3==0:
                    if count+1 ==3:
                        s1=x[-(count+1):]

                        s_split.append(s1)
                    else:
                        s1=x[-(count+1):-(count+1-3)]
                        s_split.append(s1)
                    index=count+1

                count=count+1
            final=''
            fs_split=x[:(len(x)-index)]
            for l in s_split:
                if final!='':
                    final=l+','+final
                else:
                    final=l
            if len(final)==0:
                f_final=fs_split
            elif len(fs_split)==0:
                f_final=final
            else:
                f_final=fs_split+','+final
            return f_final
        else:
            y=''.join(devanagari_nums[int(digit)] for digit in f[1])

            return x+'.'+y
    except:

        return ''.join(devanagari_nums[int(digit)] for digit in number)
