from django import forms
from accounts.models import Profile,Seller_Profile,User,City,Address
from dikko.models import Dikko,Field,Sub_Field
from products.models import Product
from orders.models import Order


class shipping_info(forms.ModelForm):
    class Meta:
        model=Order
        fields=('address',)