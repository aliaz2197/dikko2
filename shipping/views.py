from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from django.shortcuts import render, redirect
from carts.models import Cart
from accounts.models import Profile,Address,User,Reciever
from dashboard.forms import address_form
from notif.models import Notification
from orders.models import Order
from dashboard.forms import address_form
from custom_filters.templatetags import filter_numbers
from .forms import shipping_info
import mechanicalsoup
# Create your views here.
@login_required
def home(request):
    if request.is_ajax():
        print(request.POST)
        form=address_form(request.POST or None)
        if form.is_valid():
            owner=Profile.objects.get(user=request.user)
            address=form.save(commit=False)
            address.profile=owner
            address.save()
            final={"valid":1}
            return JsonResponse(final, safe=True)
        else:
            final={"error": form.errors}
            return JsonResponse(final, safe=True)
    else:
        cart_obj, cart_created = Cart.objects.new_or_get(request)
        order_obj=None
        form=address_form()
        if cart_created or cart_obj.entrys.count()==0:
            return redirect("cart:cart_home")
        else:
            pro=Profile.objects.filter(user=request.user).first()
            addresses=Address.objects.filter(profile=pro).all()
            address=addresses.first()

            if request.method == 'POST':
                print(request.POST)
                address_id=request.POST.get('address')
                addre=Address.objects.get(id=address_id)
                if addre:
                    order_obj, new_order_obj = Order.objects.get_or_create(cart=cart_obj, user=pro)
                    if addre.state=='خراسان‌جنوبی':
                        if addre.city=='بیرجند':
                            order_obj.shipping_total=4000
                        else:
                            order_obj.shipping_total=8000
                    else:
                        order_obj.shipping_total=12000
                    order_obj.address=addre
                    order_obj.save()

                    admins=Profile.objects.filter(user__admin=True)

                    if new_order_obj:
                            reciver=Reciever()
                            reciver.save()
                            for admin in admins:
                                 admin.Reciever=reciver
                                 admin.save()
                            order_obj.created_notif.create(reciver_s=reciver,actor=pro,verb='Order created')

                    return redirect('payment:home')



        return render(request,"shipping/shipping.html",{"order_obj":order_obj,"cart_obj":cart_obj,"addresses":addresses,"address":address,"form":form,
                                                        })




def calculate_shipping_cost(request):
    if request.is_ajax():

        # browser = mechanicalsoup.StatefulBrowser()
        # browser.open("http://parcelprice.post.ir/Default.aspx")
        # submit = browser.get_current_page().find('input', id='btnnext')
        # form = browser.select_form()
        # form.form.find("input", {"name": "txtWeight"})["value"] = "2000"
        # form.choose_submit(submit)
        # browser.submit_selected()
        # browser.get_current_page().find("input", {"name": "g3", "value": "rdoBetweenCity"})["checked"] = ""
        # browser.get_current_page().find("select", {"name": "cboFromState", "value": "30"})
        # browser.get_current_page().find("select", {"name": "cboFromCity", "value": "971"})
        # browser.get_current_page().find("select", {"name": "cboToState", "value": "1"})
        # browser.get_current_page().find("select", {"name": "cboToCity", "value": "1"})
        # browser.get_current_page().find("input", {"name": "chkSenderPCode"})["checked"] = ""
        # browser.get_current_page().find("input", {"name": "chkReceiverPCode"})["checked"] = ""
        # submit2 = browser.get_current_page().find('input', id='btnnext')
        # form2 = browser.select_form()
        # form2.choose_submit(submit2)
        # browser.submit_selected()
        # submit3 = browser.get_current_page().find('input', id='btnnext')
        # form3 = browser.select_form()
        # form3.choose_submit(submit3)
        # print(browser.submit_selected().text)
        # page = browser.get("http://parcelprice.post.ir/Default.aspx")
        # form = page.soup.form
        # form.find("input", {"name": "g1", "value": "rdoEMS"})["checked"] = ""
        # form.find("input", {"name": "g2", "value": "rdoPackage"})["checked"] = ""
        # form.find("input", {"name": "txtWeight"})["value"] = "2000"
        # submit = form.select('input[value="مرحله بعد"]')[0]
        # response = browser.submit(form, page.url)
        # print(response.soup)

        # submit = form.find('input', id='btnnext')
        # print("dfsfdcsdf")
        # form.choose_submit(submit)
        # response=browser.submit_selected()
        # print(response)
        # response = browser.submit(form, page.url)
        # print("responseeeeeeeeeeeeeeeeeeeee")
        # print(response)
        # form2=response.soup.form
        # print("kirrrrrrr")
        # print(form.find("input", {"name": "g3", "value": "rdoBetweenCity"}))
        # browser["q"] = "MechanicalSoup"
        # browser.submit_selected()
        #
        # # Display the results
        # for link in browser.get_current_page().select('a.result__a'):
        #     print("hello")
        #     print(link.text, '->', link.attrs['href'])
        if request.GET.get('address_id'):
            address_id=request.GET.get('address_id')
            cart_subtotal=request.GET.get('cart')
            print(cart_subtotal)
            print(address_id)
            address_obj=Address.objects.get(id=address_id)
            print(address_obj.city)
            if (address_obj.state=='خراسان‌جنوبی'):
                if (address_obj.city=='بیرجند'):
                    total=int(float(cart_subtotal))+4000
                    total=filter_numbers.persian_int(total)+" تومان "
                    final={"cost":"۴۰۰۰ تومان","total":total}
                    return JsonResponse(final,safe=True)
                else:
                    total=int(float(cart_subtotal))+8000
                    total=filter_numbers.persian_int(total)+" تومان "
                    final = {"cost": "۸۰۰۰ تومان","total":total}
                    return JsonResponse(final, safe=True)
            else:
                total=int(float(cart_subtotal))+12000
                total=filter_numbers.persian_int(total)+" تومان "
                final = {"cost": "۱۲۰۰۰ تومان","total":total}

                return JsonResponse(final, safe=True)
        else:
            final={}

            return JsonResponse(final, safe=True)