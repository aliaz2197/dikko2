from django.urls import path, include, re_path
from . import views

app_name="shipping"
urlpatterns=[
    path('',views.home,name='home'),
    path('calculate/',views.calculate_shipping_cost,name='calculate'),
]