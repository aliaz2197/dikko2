from django.http import JsonResponse
from django.shortcuts import render
from django.views.generic import TemplateView
from accounts.models import Profile
from orders.models import Order
from django.db.models import Count


# Create your views here.
class home(TemplateView):
    template_name='dashboard_analytics.html'

    def get_context_data(self, **kwargs):
        context=super().get_context_data(**kwargs)
        profile=Profile.objects.get(user=self.request.user)
        context['profile']=profile
        return context


def sell_per_product(request):
    data=Order.objects.values('cart__entrys__product__title').filter(status='پرداخت شده') \
        .annotate(total=Count('cart__entrys__product__title'))
    print(data)
    chart={
        'chart': {'type': 'pie'},
        'title': {'text': 'فروش هر محصول', 'useHTML': 'true', 'style': {
            'fontSize': '15px',
            'fontFamily': 'iran',
        }, },
        'plotOptions': {
            'pie': {
                'allowPointSelect': 'true',
                'cursor': 'pointer',
                'dataLabels': {
                    'style': {
                        'fontSize': '15px',
                        'fontFamily': 'iran',
                    },
                    'useHTML': 'true',
                },
            },
        },
        'series': [{
            'name': ' quantity',
            'useHTML': 'true',
            'data': list(map(
                lambda row: {'name': row['cart__entrys__product__title'], 'y': row['total']}, data))
        }]
    }
    return JsonResponse(chart, safe=True)


def sell_per_dikko(request):
    data=Order.objects.values('cart__entrys__product__dikko_name').filter(status='پرداخت شده') \
        .annotate(total=Count('cart__entrys__product__dikko_name'))

    chart={
        'chart': {'type': 'pie'},
        'title': {'text': 'فروش هر دیکو', 'useHTML': 'true', 'style': {
            'fontSize': '15px',
            'fontFamily': 'iran',
        }, },
        'plotOptions': {
            'pie': {
                'allowPointSelect': 'true',
                'cursor': 'pointer',
                'dataLabels': {
                    'style': {
                        'fontSize': '15px',
                        'fontFamily': 'iran',
                    },
                    'useHTML': 'true',
                },
            },
        },
        'series': [{
            'name': ' quantity',
            'useHTML': 'true',
            'data': list(map(
                lambda row: {'name': row['cart__entrys__product__dikko_name'], 'y': row['total']}, data))
        }]
    }

    return JsonResponse(chart, safe=True)
