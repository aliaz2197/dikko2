from django.db import models
from django.conf import settings
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from .signals import object_viewed_signal
from .utils import get_client_ip
from django.contrib.sessions.models import Session
from products.models import Product
from django.db.models.signals import pre_save,post_save
# Create your models here.
User=settings.AUTH_USER_MODEL
class CountViewedObject(models.Model):
    counter=models.PositiveIntegerField(default=0)
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')

    class Meta:
        ordering=['-counter']

class ObjectViewed(models.Model):
    user=models.ForeignKey(User,blank=True,null=True,on_delete=models.SET_NULL)
    ip_address = models.CharField(max_length=220,blank=True,null=True)
    content_type=models.ForeignKey(ContentType,on_delete=models.CASCADE)
    object_id=models.PositiveIntegerField()
    content_object=GenericForeignKey('content_type','object_id')
    timestamp=models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return "%s Viewed on %s"%(self.content_object,self.timestamp)
    class Meta:
        ordering=['-timestamp']
        verbose_name='Object Viewed'
        verbose_name_plural='Objects viewed'


def object_viewed_receiver(sender,instance,request,*args,**kwargs):

    product=False
    for base in sender.__bases__:
        if base.__name__ == 'Product':
            product=True
            c_type=ContentType.objects.get_for_model(Product)
    if product is False:
        c_type=ContentType.objects.get_for_model(sender)
    if ObjectViewed.objects.filter(content_type__model='product',object_id=instance.id).count()>0:
        obj=CountViewedObject.objects.get(content_type__model='product',object_id=instance.id)
        obj.counter=obj.counter+1
        obj.save()
    else:
        obj=CountViewedObject.objects.create(
            content_type=c_type,
            object_id=instance.id,
            counter=1
        )
    print(request.user)
    if request.user.is_authenticated:
        new_viewed_obj=ObjectViewed.objects.create(
            user=request.user,
            content_type=c_type,
            object_id=instance.id,
            ip_address=get_client_ip(request)
        )
    else:
        new_viewed_obj=ObjectViewed.objects.create(
            # user=request.user,
            content_type=c_type,
            object_id=instance.id,
            ip_address=get_client_ip(request)
        )

object_viewed_signal.connect(object_viewed_receiver)

