from django.contrib import admin
from .models import ObjectViewed,CountViewedObject
# Register your models here.
admin.site.register(ObjectViewed)
admin.site.register(CountViewedObject)