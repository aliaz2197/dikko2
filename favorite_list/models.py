from django.db import models
from django.utils import timezone

from products.models import Product
# Create your models here.
from dikko2 import settings


class Favorite(models.Model):
    user=models.OneToOneField(settings.AUTH_USER_MODEL,
                             on_delete=models.CASCADE,unique=True)
    products=models.ManyToManyField(Product,blank=True)
    created_date=models.DateTimeField(auto_now_add=timezone.localtime(timezone.now()))
    modified_date=models.DateTimeField(auto_now=timezone.localtime(timezone.now()))
    def __str__(self):
        return self.user.email