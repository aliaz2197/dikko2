from django.urls import path,include,re_path
from .views import add_to_list,check_list


app_name='favorite_list'

urlpatterns=[
  path('add/',add_to_list,name='add_favorite'),
    path('check/',check_list,name='check_list'),
]

