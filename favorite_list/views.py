from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import JsonResponse
from django.shortcuts import render

from accounts.models import Profile
from products.models import Product
from .models import Favorite
from django.views.generic import ListView

# Create your views here.

class Favorite_List(LoginRequiredMixin,ListView):
    template_name = 'dashboard-favorite-list.html'
    paginate_by = 8
    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        pro = Profile.objects.filter(user=self.request.user).first()
        context['profile'] = pro
        return context

    def get_queryset(self):
        user=self.request.user
        try:
            favorite=Favorite.objects.get(user=user)
            return favorite.products.all()
        except:
            return Favorite.objects.none()


def check_list(request):
    if request.is_ajax():
        if request.user.is_authenticated:
            product_id = request.GET.get('product_id')
            product_obj = Product.objects.get(id=product_id)
            print(product_obj)

            try:

                    favorite = Favorite.objects.get(user=request.user)
                    print(favorite)
                    if favorite.products.count() > 0:
                        for product in favorite.products.all():
                            if product == product_obj:

                                final = {"valid": 0}
                                return JsonResponse(final, safe=True)


                        final = {"valid": 1}
                        return JsonResponse(final, safe=True)
                    else:


                        final = {"valid": 1}
                        return JsonResponse(final, safe=True)
            except:
                final = {"valid": 1}
                return JsonResponse(final, safe=True)
        else:
            final={"valid": 2}
            return JsonResponse(final, safe=True)

def add_to_list(request):
    if request.is_ajax():
        if request.user.is_authenticated:
            print("fdfv")
            product_id=request.GET.get('product_id')
            product_obj=Product.objects.get(id=product_id)
            print(product_obj)

            try:
                favorite=Favorite.objects.get(user=request.user)
                print(favorite)
            except:
                favorite=Favorite(user=request.user)
                favorite.save()
            print("hello")
            print(request.GET.get("check"))
            if favorite.products.count() >0:
                for product in favorite.products.all():
                    if product==product_obj:

                            favorite.products.remove(product)
                            favorite.save()
                            final={"valid":0}
                            return JsonResponse(final,safe=True)

                favorite.products.add(product_obj)
                favorite.save()
                final = {"valid": 1}
                return JsonResponse(final, safe=True)
            else:

                favorite.products.add(product_obj)
                favorite.save()
                final = {"valid": 1}
                return JsonResponse(final, safe=True)
        else:
            final={"valid": 2}
            return JsonResponse(final, safe=True)