from django.views.generic import TemplateView
from django.shortcuts import render
from accounts.models import Profile, Seller_Profile
from products.models import Product
from dikko.models import Dikko, Sub_Field
from analytics.models import CountViewedObject
from django.http import JsonResponse
from django.core.mail import send_mail


class HomePage(TemplateView):
    template_name='index.html'

    def get_context_data(self, **kwargs):
        context=super().get_context_data(**kwargs)

        products=Product.objects.all()
        handCrafts_dikko=Dikko.objects.filter(field__name='صنایع دستی').all()
        handCrafts=[]
        most_viewed_products=[]
        try:
            if CountViewedObject.objects.count()>8:
                for obj in CountViewedObject.objects.all()[:8]:
                    if Product.objects.filter(id=obj.object_id).exists():
                        pro=Product.objects.get(id=obj.object_id)
                        print(pro)
                        most_viewed_products.append(pro)

            else:
                for obj in CountViewedObject.objects.all():
                    if Product.objects.filter(id=obj.object_id).exists():
                        pro=Product.objects.get(id=obj.object_id)
                        most_viewed_products.append(pro)


        except:
            print("errors")
        dikko_s=[]
        sfield=Sub_Field.objects.all()
        for sf in sfield:
            dikko_s.append(sf.dikko_set.all())

        context['products']=products
        context['dikko_s']=dikko_s
        # context['sfield']=sfield.name
        context['mostViewed']=most_viewed_products
        return context


def seller_page(request, dikkourl):
    profile=Profile.objects.get(user=request.user)

    return render(request, 'profile_seller/profile.html', {'profile': profile})


class aboutUs(TemplateView):
    template_name='aboutUs.html'


class termsCondition(TemplateView):
    template_name='termsCondition.html'
