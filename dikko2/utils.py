from django.utils.text import slugify
import random
import string
from django.utils.timezone import now

def random_string_generator(size=10, chars='ابپتثجچحخدذرزژسشصضطظعغفقکگلمنوهی' + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))
def random_string_generator2(size=10, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

def random_string_generator3(size=8, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

def unique_order_id_generator(instance, new_slug=None):

    order_new_id=random_string_generator2()
    Klass = instance.__class__
    qs_exists = Klass.objects.filter(order_id=order_new_id).exists()
    if qs_exists:
        return unique_order_id_generator(instance)
    return order_new_id
def unique_complain_id_generator(instance, new_slug=None):

    complain_new_id=random_string_generator2()
    Klass = instance.__class__
    qs_exists = Klass.objects.filter(complain_id=complain_new_id).exists()
    if qs_exists:
        return unique_complain_id_generator(instance)
    return complain_new_id
def unique_product_code_generator(instance,dikko_obj, new_slug=None):
    product_pre_new_code=random_string_generator3()
    print("preeeeee")
    print(product_pre_new_code)
    sfield=dikko_obj.sfield.first()
    product_new_code=sfield.scode+'-'+product_pre_new_code
    print("newwwwww")
    print(product_new_code)
    Klass = instance.__class__
    qs_exists = Klass.objects.filter(code=product_new_code).exists()
    if qs_exists:
        return unique_product_code_generator(instance)
    return product_new_code
def unique_slug_generator(instance, new_slug=None):
    """
    This is for a Django project and it assumes your instance
    has a model with a slug field and a title character (char) field.
    """
    if new_slug is not None:
        slug = new_slug
    else:
        try:
            slug=slugify(instance.title,allow_unicode=True)
        except:
            slug = slugify(instance.name, allow_unicode=True)



    Klass = instance.__class__
    qs_exists = Klass.objects.filter(slug=slug).exists()
    if qs_exists:
        new_slug = "{slug}-{randstr}".format(
                    slug=slug,
                    randstr=random_string_generator(size=4)
                )
        return unique_slug_generator(instance, new_slug=new_slug)
    return slug




