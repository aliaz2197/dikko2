"""dikko2 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
import notifications.urls
from django.contrib import admin
from django.urls import path,include
from . import views
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.sitemaps.views import sitemap
from products.sitemaps import ProductSitemap
sitemaps = {
    'products': ProductSitemap
}
app_name='dikko2'
urlpatterns = [
    path('sitemap.xml', sitemap, {'sitemaps': sitemaps},
     name='sitemap'),
    path('ali/admin/', admin.site.urls),
    path('', views.HomePage.as_view(), name='home'),
    path('accounts/', include('accounts.urls'), name='accounts'),
    path('oauth/', include('allauth.urls')),
    path('dashboard/',include('dashboard.urls'),name='dashboard'),
    path('cart/',include('carts.urls'),name='carts'),
    path('discount_codes/', include('discount_codes.urls'), name='discount_codes'),
    path('comment/', include('comments.urls'), name='comments'),
    path('shipping/', include('shipping.urls'), name='shipping'),
    path('products/',include('products.urls'),name='products'),
    path('dikko/',include('dikko.urls'),name='dikko'),
    path('payment/',include('payment.urls'),name='payment'),
    path('tinymce/', include('tinymce.urls')),
    path('notifications/', include('notif.urls'),name='notif'),
    path('search/',include('search.urls'),name='search'),
    path('favorite_list/',include('favorite_list.urls'),name='favorite_list'),
    # path('test/',views.test,name='test'),
    # path('test2/',views.test2,name='test2'),
    # path('webpage/',views.webpage,name='webpage'),
    path('accounts/',include('django.contrib.auth.urls')),
    path('aboutus/',views.aboutUs.as_view(),name='aboutUs'),
    path('contactUs/',include('contactUs.urls'),name='contactUs'),
    path('termscondition/',views.termsCondition.as_view(),name='termsCondition'),
    path('complaints/',include('complaints.urls'),name='complaints'),
    path('subscribers/',include('subscribers.urls'),name='subscribers'),
    # path('inbox/notifications/', include(notifications.urls, namespace='notifications')),
    # path('<str:dikkourl>/', include('profile_seller.urls'), name='profile_seller'),
    # path('',cart_detail_api_view,name="cart-api")

]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
