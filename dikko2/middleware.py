from django.utils.timezone import now
from django.utils.deprecation import MiddlewareMixin
from accounts.models import User

class SetLastVisitMiddleware(MiddlewareMixin):
    def process_response(self, request, response):
            if request.user.is_authenticated:
                if not request.user.is_staff:
                    # Update last visit time after request finished processing.

                    print(User.objects.filter(pk=request.user.pk).first().last_visit)
                    User.objects.filter(pk=request.user.pk).update(last_visit=now())
            return response