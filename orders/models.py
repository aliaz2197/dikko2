import math
from django.db import models
from django.dispatch import receiver

from carts.models import Cart
from dikko2.utils import unique_order_id_generator
from django.db.models.signals import pre_save,post_save
from accounts.models import Profile,Address,User
from django.utils import timezone
from notif.models import Notification
from django.contrib.contenttypes.fields import GenericRelation
from notifications.signals import notify
ORDER_STATUS_CHOICES=(
    ('اولیه','اولیه'),
    ('پرداخت شده','پرداخت شده'),
    ('تایید شده','تایید شده'),
    ('رد شده','رد شده'),
    ('ارسال شده','ارسال شده'),
    ('تحویل شده','تحویل شده'),
)

# Create your models here.

class Order(models.Model):
    order_id=models.CharField(max_length=120,blank=True)
    cart=models.ForeignKey(Cart,on_delete=models.CASCADE)
    status=models.CharField(max_length=120,default='اولیه',choices=ORDER_STATUS_CHOICES)
    shipping_total = models.DecimalField(default=0.00, max_digits=100, decimal_places=2)
    total=models.DecimalField(default=0.00,max_digits=100,decimal_places=2)
    user=models.ForeignKey(Profile,on_delete=models.SET_NULL,null=True,blank=True)
    address=models.ForeignKey(Address,on_delete=models.SET_NULL,null=True,blank=True)
    created_notif=GenericRelation(Notification,related_query_name='orders')
    created_date = models.DateTimeField()
    modified_date=models.DateTimeField()
    invoiceDate=models.DateTimeField(null=True)
    tracePost=models.CharField(max_length=50,null=True,blank=True)

    def __str__(self):
        return self.order_id


    def update_total(self):
        cart_total=self.cart.total
        shipping_total=self.shipping_total
        new_total=math.fsum([float(cart_total)+float(shipping_total)])
        formatted_total=format(new_total,'.2f')
        self.total=formatted_total
        self.save()
        return new_total
    def save(self, *args, **kwargs):
        ''' On save, update timestamps '''
        if not self.id:
            self.created_date = timezone.localtime(timezone.now())
        self.modified_date =timezone.localtime(timezone.now())
        cart_total=self.cart.total
        shipping_total=self.shipping_total
        new_total=math.fsum([float(cart_total)+float(shipping_total)])
        formatted_total=format(new_total, '.2f')
        self.total=formatted_total

        return super(Order, self).save(*args, **kwargs)


def pre_save_create_order_id(sender,instance,*args,**kwargs):
    if not instance.order_id:
        instance.order_id=unique_order_id_generator(instance)


pre_save.connect(pre_save_create_order_id,sender=Order)

def post_save_cart_total(sender,instance,created,*args,**kwargs):
    if not created:
        cart_obj=instance
        cart_total=cart_obj.total
        cart_id=cart_obj.id
        qs=Order.objects.filter(cart__id=cart_id)
        if qs.count()==1:
            order_obj=qs.first()
            order_obj.update_total()

post_save.connect(post_save_cart_total,sender=Cart)

# def post_save_order(sender,instance,created,*args,**kwargs):
#     # if created:
#         instance.update_total()

# post_save.connect(post_save_order,sender=Order)

# @receiver(post_save, sender=Order)
# def order_id_setter(sender, instance, created, *args, **kwargs):
#  	if created:
#         # print(instance)
#  	    notif(user=instance.user,
#  				actor=f"Order {instance.order_id}", verb=" placed 		Successfully.")

# def my_handler(sender, instance, created, **kwargs):
#
#
# post_save.connect(my_handler, sender=Order)