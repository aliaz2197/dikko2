from django.apps import AppConfig


class BecomeArtistConfig(AppConfig):
    name = 'become_artist'
