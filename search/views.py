from django.shortcuts import render
from products.models import Product
from dikko.models import Dikko
from django.db.models import Q
# Create your views here.
from django.views.generic import ListView


class SearchProductView(ListView):
    template_name = 'search/search_view.html'
    paginate_by = 20
    def get_queryset(self):
        query=self.request.GET.get('q')
        select=self.request.GET.get('select')

        if query is not None:
            if select=='All':
                lookups=Q(title__icontains=query)|Q(description__icontains=query)
                return Product.objects.filter(lookups).distinct()
            else:


                    lookups = Q(title__icontains=query) | Q(description__icontains=query)
                    dikko_s=Dikko.objects.filter(field__name=select).all()
                    products=set()
                    search_result=set()
                    for dikko in dikko_s:
                        products.add(dikko.products.all())
                    for pro in products:
                        if pro.filter(lookups):
                            search_result.add(pro.filter(lookups))
                    return search_result

        return Product.objects.none()