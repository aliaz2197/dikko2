from django.http import HttpResponse, JsonResponse
from django.shortcuts import render
from .forms import ContactForm
from django.core.mail import EmailMessage
# Create your views here.
def home(request):
    form=ContactForm()
    if request.is_ajax():
        email=request.GET.get('email')
        print(email)
        subject=request.GET.get('subject')
        message=request.GET.get('message')
        try:
            email=EmailMessage(subject, message, 'info@boghband.com',
                               ['support@boghband.com', ], headers={'Reply-To': email})
            # send() sends the email
            email.send()
            final={'valid':1}
            return JsonResponse(final, safe=False)
        except :
            final={'valid': 0}
            print("error")
            return JsonResponse(final, safe=False)

    return render(request,'contactUs/contactUs.html',{'form':form})