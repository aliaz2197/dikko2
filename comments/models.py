from django.contrib.contenttypes.fields import GenericRelation
from django.db import models
from django.db.models.signals import post_save
from django.urls import reverse
from django.utils import timezone


from accounts.models import Profile,User,Reciever
from notif.models import Notification
from products.models import Product
# Create your models here.

class CommentManager(models.Manager):
    def count(self):
        return "10"
    # def all(self):
    #     qs=super(CommentManager,self).filter(parent=None)
    #     return qs

class Comment(models.Model):
    author=models.ForeignKey(Profile,on_delete=models.CASCADE,related_name='comments',null=True)
    product=models.ForeignKey(Product,on_delete=models.CASCADE,null=True)
    text=models.TextField()
    created_date = models.DateTimeField(auto_now_add=True)
    approved_comment = models.BooleanField(default=False)
    parent=models.ForeignKey("self",null=True,blank=True,on_delete=models.SET_NULL)
    comment_notif = GenericRelation(Notification, related_query_name='comments')
    objects=CommentManager()
    def approve(self):
        self.approved_comment = True
        self.save()

    def get_absolute_url(self):
        return self.product.get_absolute_url()

    def __str__(self):
        return self.text

    def children(self):
        return Comment.objects.filter(parent=self)

    @property
    def is_parent(self):
        if self.parent is not None:
            return False
        return True

def post_save_comment(sender,instance,created,*args,**kwargs):
    admin=Profile.objects.filter(user__admin=True).first()
    if created:
        reciever=Reciever()
        reciever.save()
        admin.Reciever=reciever
        # reciever.profile_set.add(admin.profile)
        admin.save()
        instance.comment_notif.create(reciver_s=reciever, actor=instance.author, verb='comment posted')


post_save.connect(post_save_comment, sender=Comment)