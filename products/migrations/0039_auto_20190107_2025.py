# Generated by Django 2.0.2 on 2019-01-07 16:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0038_auto_20190107_1932'),
    ]

    operations = [
        migrations.AddField(
            model_name='cimage',
            name='path',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
        migrations.AlterField(
            model_name='cimage',
            name='image',
            field=models.ImageField(blank=True, upload_to=models.CharField(blank=True, max_length=100, null=True)),
        ),
    ]
