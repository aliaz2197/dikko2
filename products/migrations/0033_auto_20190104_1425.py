# Generated by Django 2.0.2 on 2019-01-04 10:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0032_auto_20190104_1419'),
    ]

    operations = [
        migrations.AddField(
            model_name='height',
            name='height_rooyi',
            field=models.PositiveIntegerField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='height',
            name='height_ziri',
            field=models.PositiveIntegerField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='height',
            name='height_back',
            field=models.PositiveIntegerField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='height',
            name='height_front',
            field=models.PositiveIntegerField(blank=True, null=True),
        ),
    ]
