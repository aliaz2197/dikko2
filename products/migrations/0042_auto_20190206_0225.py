# Generated by Django 2.0.2 on 2019-02-05 22:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0041_product_discount'),
    ]

    operations = [
        migrations.CreateModel(
            name='DType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, max_length=50, null=True)),
            ],
        ),
        migrations.DeleteModel(
            name='Type',
        ),
        migrations.AlterField(
            model_name='dress',
            name='type',
            field=models.ManyToManyField(to='products.DType'),
        ),
    ]
