# Generated by Django 2.0.2 on 2019-01-07 21:10

from django.db import migrations, models
import products.models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0039_auto_20190107_2025'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cimage',
            name='image',
            field=models.ImageField(blank=True, null=True, upload_to=products.models.get_upload_path),
        ),
    ]
