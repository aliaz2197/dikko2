# Generated by Django 2.0.2 on 2019-04-11 18:05

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0051_auto_20190308_0022'),
    ]

    operations = [
        migrations.CreateModel(
            name='Dast_Saze',
            fields=[
                ('product_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='products.Product')),
                ('dimensions', models.CharField(blank=True, max_length=20)),
                ('madeIn', models.CharField(blank=True, max_length=40)),
                ('weight', models.PositiveIntegerField(blank=True, default=0)),
                ('type', models.CharField(max_length=20)),
                ('color', models.ManyToManyField(blank=True, to='products.Color')),
                ('suitble_age', models.ManyToManyField(blank=True, to='products.Age')),
                ('suitble_for', models.ManyToManyField(blank=True, to='products.SuitbleFor')),
            ],
            bases=('products.product',),
        ),
    ]
