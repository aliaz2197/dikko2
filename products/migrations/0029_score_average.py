# Generated by Django 2.0.2 on 2018-12-10 09:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0028_score_count'),
    ]

    operations = [
        migrations.AddField(
            model_name='score',
            name='average',
            field=models.PositiveIntegerField(blank=True, default=0),
        ),
    ]
