from django.contrib import admin
from .models import Product,Score,Dress,Color,Size,Height,SuitbleFor,Age,TypeSewing,DType,CImage,RuDuzi

# Register your models here.
admin.site.register(RuDuzi)
admin.site.register(Product)
admin.site.register(Score)
admin.site.register(Dress)
admin.site.register(Color)
admin.site.register(Size)
admin.site.register(Height)
admin.site.register(SuitbleFor)
admin.site.register(Age)
admin.site.register(TypeSewing)
admin.site.register(DType)
admin.site.register(CImage)
# admin.site.register(Type)