from django.urls import path,include,re_path
from django.contrib.auth import views as auth_views
from products.views import ProductDetails,calculate_score
from django.conf.urls import url


app_name='products'

urlpatterns=[
   # path('handcrafts/',Main_hand_crafts.as_view(),name='main_hand_crafts'),
   re_path(r'^(?P<dikko>[\w-]+)/(?P<slug>[\w-]+)/$',ProductDetails.as_view(),name='product_details'),
   path('score/',calculate_score,name='calculate_score'),
]

