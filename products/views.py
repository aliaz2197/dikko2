from django.http import Http404, HttpResponseRedirect, JsonResponse
from django.shortcuts import render, get_object_or_404, redirect
from django.urls import reverse
from django.views.generic import DetailView,ListView
from carts.models import Cart
from products.models import Product,Score,Dress,Zivar,RuDuzi,Dast_Saze
from comments.models import Comment
from comments.forms import commentForm
from django.views.generic.edit import FormMixin
from accounts.models import Profile,Seller_Profile
from dikko.models import Dikko
from analytics.mixins import ObjectViewedMixin
from django.db.models import Q
# Create your views here.

class ProductDetails(FormMixin,ObjectViewedMixin,DetailView):
    template_name ='products/product_detail.html'
    queryset =Product.objects.all()
    form_class = commentForm

    def get_success_url(self):
        slug=self.get_object().slug
        dikko = self.get_object().dikko_set.first()
        return reverse("products:product_details",kwargs={"slug":slug,"dikko":dikko.slug})

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)

        else:
            return self.form_invalid(form)

    def form_valid(self, form):
        comment=Comment()
        comment.text=form.cleaned_data.get('text')
        comment.product=self.get_object()
        comment.author=Profile.objects.filter(user=self.request.user).first()
        if self.request.user.is_admin:
            comment.approve()
        parent_id=self.request.POST.get("parent_id")
        if parent_id:
            parent_qs=Comment.objects.filter(id=parent_id)
            if parent_qs.exists():
                parent_obj=parent_qs.first()
                comment.parent=parent_obj
        comment.save()
        return super(ProductDetails, self).form_valid(form)


    def get_context_data(self,*args, **kwargs):
        context=super(ProductDetails,self).get_context_data(**kwargs)
        cart_obj, new_obj = Cart.objects.new_or_get(self.request)
        slug = self.kwargs.get('slug')
        instance = Product.objects.get(slug=slug)
        dikko=Dikko.objects.filter(name=instance.dikko_name).first()
        comments=Comment.objects.filter(product=instance,approved_comment=True).filter(parent=None).all()
        context['comments_count']=Comment.objects.filter(product=instance,approved_comment=True).all().count()
        context['comments']=comments
        context['cart']=cart_obj
        context['dikko']=dikko
        context['product']=instance
        print("sdsdsssssssssssssssssssssssssssssssssss")
        # print(self.get_object().__class__)
        context['class']=None
        print("productt")
        if isinstance(self.get_object(),Dress):

            obj=self.get_object()
            print(obj.color.all())
            context['class']='Dress'
            context['obj']=obj
        elif isinstance(self.get_object(),Zivar):
            print("zivarrrrrr")
            obj=self.get_object()
            print(obj.color.all())
            context['class']='Zivar'
            context['obj']=obj
        elif isinstance(self.get_object(),RuDuzi):
            print("zivarrrrrr")
            obj=self.get_object()
            print(obj.color.all())
            context['class']='RuDuzi'
            context['obj']=obj

        elif isinstance(self.get_object(), Dast_Saze):
            print("dastttsazzeee")
            obj=self.get_object()
            print(obj.color.all())
            context['class']='Dast_Saze'
            context['obj']=obj

        return context
    def get_object(self, *args,**kwargs):
        request=self.request
        slug=self.kwargs.get('slug')
        dikko=self.kwargs.get('dikko')

        try:
            dikko=Dikko.objects.get(slug=dikko)
            print("sssssssssss")
            for sf in dikko.sfield.all():
                if sf.scode[0]=='D':
                    if sf.scode[1]=='H':
                        instance=Dast_Saze.objects.get(slug=slug)
                    else:
                        instance=Dress.objects.get(slug=slug)
                elif sf.scode[0]=='H':
                    if sf.scode[1]=='J':
                        instance=Zivar.objects.get(slug=slug)
                    elif sf.scode[1]=='R':
                        instance=RuDuzi.objects.get(slug=slug)




        except Product.DoesNotExist:
            raise Http404("Not Found..")

        return instance
def calculate_score(request):
    if request.is_ajax():
        if request.user.is_authenticated:
            score=request.GET.get('score')
            id = request.GET.get('product_id')
            product=Product.objects.get(id=id)
            score_obj=Score.objects.create(value=score,product=product)
            score_obj.save()
            scores=product.score_set.all()
            sum=0
            for sco in scores:
                sum=sum+sco.value

            product.avg_score=sum/scores.count()
            product.save()
            final={'ok':score_obj.score}
            return JsonResponse(final,safe=False)