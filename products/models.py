import random
import string

from django.utils import timezone

from custom_filters.templatetags.filter_numbers import persian_int
from django.db import models
from django.urls import reverse
from dikko2.utils import unique_slug_generator
from django.db.models.signals import pre_save,post_save
from PIL import Image
from io import BytesIO
from django.core.files.uploadedfile import InMemoryUploadedFile
import sys
from tinymce.models import HTMLField
from taggit.managers import TaggableManager

# Create your models here.

class Color(models.Model):
    name=models.CharField(max_length=20)
    def __str__(self):
        return self.name

class Size(models.Model):
    name=models.CharField(max_length=20)
    def __str__(self):
        return persian_int(self.name)
class Height(models.Model):#### GHAD LEBAS
    height_front=models.PositiveIntegerField(null=True,blank=True)
    height_back=models.PositiveIntegerField(null=True,blank=True)
    height_ziri=models.PositiveIntegerField(null=True,blank=True)
    height_rooyi=models.PositiveIntegerField(null=True,blank=True)
    def __str__(self):
        if self.height_front is not None:
            return f"جلو:{persian_int(self.height_front)} پشت{persian_int(self.height_back)} "
        else:

            return f"رویی{persian_int(self.height_rooyi)} پشتی{persian_int(self.height_ziri)}"
# class Type(models.Model):  #### Jens
#     name=models.CharField(max_length=50)
#     def __str__(self):
#         return self.name
class DType(models.Model):
    name=models.CharField(max_length=50,null=True,blank=True)
    def __str__(self):
        return self.name
class SuitbleFor(models.Model): ### for example: Monaseb baraye Zemestan
    name=models.CharField(max_length=100)
    def __str__(self):
        return self.name
class TypeSewing(models.Model):  ### Noe Tolidi
    name = models.CharField(max_length=150)
    def __str__(self):
        return self.name
class Age(models.Model):
    name=models.CharField(max_length=50)
    def __str__(self):
        return self.name
class Dimensions(models.Model):
    width=models.PositiveIntegerField()
    lenght = models.PositiveIntegerField()
    height = models.PositiveIntegerField(blank=True)

    def __str__(self):
        return f"{self.lenght} * {self.width}*{self.height}"


class ProductQuerySet(models.QuerySet):

    def active(self):
        return self.filter(active=True)

class ProductManager(models.Manager):
    def get_queryset(self):
        return ProductQuerySet(self.model,using=self._db)

    def active(self):
        return self.get_queryset().active()

    def get_by_id(self,id):
        qs=self.get_queryset().filter(id=id)
        if qs.count()==0:
            return qs.first()
        return None

def get_upload_path(instance, filename):
        x=instance.path
        return x
        # print(instance.product_set.all())
        # return 'products/img/{0}/{1}/{2}'.format(instance.dikko_name,instance.title,filename)
class CImage(models.Model):
    path=models.CharField(max_length=100,blank=True,null=True)
    image=models.ImageField(upload_to=get_upload_path,blank=True,null=True)
    def save(self):
        if not self.image:
            try:
                im=Image.open(self.image)
                output=BytesIO()
                im=im.resize((679, 550))
                if im.mode in ("RGBA", "P"):
                    im = im.convert("RGB")
                im.save(output, format='JPEG', quality=100)
                output.seek(0)
                self.image=InMemoryUploadedFile(output, 'ImageField', "%s.jpg" % self.image.name.split('.')[0],
                                                'image/jpeg',
                                                sys.getsizeof(output), None)
            except:
                print("exceptionn")
        super(CImage, self).save()
class Product(models.Model):
    title=models.CharField(max_length=120)
    code=models.CharField(max_length=12,blank=True)
    description=HTMLField('Description')
    slug=models.SlugField(blank=True,unique=True,allow_unicode=True)
    price=models.DecimalField(decimal_places=2,max_digits=20)
    image=models.ManyToManyField(CImage)
    seller=models.ForeignKey('accounts.Profile',on_delete=models.SET_NULL,null=True,related_name='product')
    is_active=models.BooleanField(default=False)
    available_quantity=models.IntegerField(blank=True,default=1)
    avg_score=models.PositiveIntegerField(default=0)
    purchaced=models.PositiveIntegerField(default=0,blank=True)
    dikko_name=models.CharField(max_length=20,blank=True,null=True)
    dikko_slug=models.CharField(max_length=30,blank=True,null=True)
    tags = TaggableManager()
    discount=models.DecimalField(decimal_places=2,max_digits=10,null=True,blank=True)
    created_date = models.DateTimeField()
    modified_date=models.DateTimeField()
    objects=ProductManager()
    def save(self, *args, **kwargs):
        ''' On save, update timestamps '''
        if not self.id:
            self.created_date = timezone.localtime(timezone.now())
        self.modified_date =timezone.localtime(timezone.now())
        return super(Product, self).save(*args, **kwargs)
    def get_absolute_url(self):
        print(self.title)
        dikko=self.dikko_set.first()
        print(dikko)
        return reverse("products:product_details",kwargs={"slug":self.slug,"dikko":dikko.slug})
    def __str__(self):
        return self.title


def product_pre_receiver(sender,instance,*args,**kwargs):
    if not instance.slug:
        instance.slug=unique_slug_generator(instance)



def change_cart_total_on_product_change(sender,instance,created,*args,**kwargs):
    if not created:
        if not instance.is_active:
            print(instance)
            entrys=instance.entry_set.all()
            print("proooooooooooooooooooooooooooo")
            print(entrys)
            for rn in entrys:
                    cr=rn.cart_set.all()
                    print(cr)
                    rn.delete()
                    for c in cr:
                        print(c)
                        c.save()


pre_save.connect(product_pre_receiver,sender=Product)
post_save.connect(change_cart_total_on_product_change,sender=Product)

class Score(models.Model):
    value = models.PositiveIntegerField(default=0, blank=True,null=True)
    product=models.ForeignKey(Product,blank=True,null=True,on_delete=models.CASCADE)


class Dress(Product):

    color=models.ManyToManyField(Color)
    size=models.ManyToManyField(Size)
    height=models.ManyToManyField(Height)
    type =models.ManyToManyField(DType)
    suitble_for=models.ManyToManyField(SuitbleFor)
    suitble_age=models.ManyToManyField(Age)
    type_sewing=models.ManyToManyField(TypeSewing)

    def __str__(self):
        return Product.__str__(self)





pre_save.connect(product_pre_receiver,sender=Dress)

class Zivar(Product):
    color=models.ManyToManyField(Color,blank=True)
    madeIn=models.CharField(max_length=40)
    suitble_for=models.ManyToManyField(SuitbleFor,blank=True)
    suitble_age=models.ManyToManyField(Age,blank=True)
    weight=models.PositiveIntegerField(default=0,blank=True)
    type=models.CharField(max_length=20)
    def __str__(self):
        return Product.__str__(self)

pre_save.connect(product_pre_receiver,sender=Zivar)

class RuDuzi(Product):
    dimensions=models.CharField(max_length=20,blank=True)
    color=models.ManyToManyField(Color,blank=True)
    madeIn=models.CharField(max_length=40,blank=True)
    suitble_for=models.ManyToManyField(SuitbleFor,blank=True)
    suitble_age=models.ManyToManyField(Age,blank=True)
    weight=models.PositiveIntegerField(default=0, blank=True)
    type=models.CharField(max_length=20)
    def __str__(self):
        return Product.__str__(self)
pre_save.connect(product_pre_receiver,sender=RuDuzi)



class Dast_Saze(Product):
    dimensions=models.CharField(max_length=20, blank=True)
    color=models.ManyToManyField(Color, blank=True)
    suitble_for=models.ManyToManyField(SuitbleFor, blank=True)
    madeIn=models.CharField(max_length=40, blank=True)
    suitble_age=models.ManyToManyField(Age, blank=True)
    weight=models.PositiveIntegerField(default=0, blank=True)
    type=models.CharField(max_length=20,blank=True)
    def __str__(self):
        return Product.__str__(self)
pre_save.connect(product_pre_receiver,sender=Dast_Saze)


