from django.db import models
from django.conf import settings
from products.models import Product
from django.db.models.signals import pre_save,post_save,m2m_changed,post_delete
User=settings.AUTH_USER_MODEL
# Create your models here.

class EntryManager(models.Manager):
    def get_queryset(self):
        return super(EntryManager, self).get_queryset().filter(product__is_active=True)

class Entry(models.Model):
    product=models.ForeignKey(Product,on_delete=models.CASCADE,blank=True)
    quantity=models.PositiveIntegerField(blank=True)
    total=models.PositiveIntegerField(blank=True)
    color=models.CharField(max_length=20,blank=True,null=True)
    size=models.CharField(max_length=3, blank=True, null=True)
    # objects=EntryManager()
    def __str__(self):
        return self.product.title

class CartManager(models.Manager):

    def new_or_get(self,request):

        cart_id = request.session.get("cart_id", None)
        qs = self.get_queryset().filter(id=cart_id)
        if qs.count() == 1:
            new_obj=False
            cart_obj = qs.first()
            if request.user.is_authenticated and cart_obj.user is None:
                cart_obj.user = request.user
                cart_obj.save()
        else:
            new_obj=True
            cart_obj = self.new(user=request.user)
            request.session['cart_id'] = cart_obj.id

        return cart_obj,new_obj

    def new(self,user=None):
        user_obj=None
        if user is not None:
            if user.is_authenticated:
                user_obj=user
        return self.model.objects.create(user=user_obj)
class Cart(models.Model):
    user=models.ForeignKey(User,null=True,blank=True,on_delete=models.SET_NULL)
    entrys=models.ManyToManyField(Entry,blank=True)
    subtotal=models.DecimalField(default=0.00,max_digits=1000,decimal_places=2)
    total=models.DecimalField(default=0.00,max_digits=1000,decimal_places=2)
    updated=models.DateTimeField(auto_now=True)
    timestamp=models.DateTimeField(auto_now_add=True)
    has_discount=models.BooleanField(default=False)
    old_total=models.DecimalField(default=0.00,max_digits=1000,decimal_places=2)
    # entry = models.ForeignKey(Cart, blank=True, on_delete=models.CASCADE)
    objects=CartManager()
    def __str__(self):
        return str(self.id)


def m2m_change_cart_reciver(sender,instance,action,*args,**kwargs):
    if action == 'post_add' :
        total=0
        print("instanceeeeeeeeeeeeee")

        print(instance)
        print(sender)
        entrys=instance.entrys.all()
        # cart_obj=Cart.objects.filter(entrys=instance).first()
        print("cart_obj kirrrrrrrrrrrrrrrrrrrrrrr")
        # print(cart_obj)
        # if cart_obj is not None:
        #     if cart_obj.entrys.all().count() > 0:
        for en in entrys:
                    print("entry total")
                    print(total)
                    total =total+en.total
                    print("after sum")
                    print(total)

                    print("totalll kossssssssssssssssssssssssssssssss")
                    print(total)

                    instance.subtotal=total
                    instance.save()
    # else:
        # cart_obj.subtotal=instance.product.price


    #     print("preeeeeeeeeeeeee")
    #     print(action)
    # # if action=='post_add' or action=='post_remove' or action=='post_clear':
    #     print("mmm2222mmmmmmmm")
    #     print(sender)
    #     # print(instance)
    #     print(instance)
    #     print("finish")
def post_save_cart_reciver(sender,instance,created,*args,**kwargs):
    print("postttttttttttttttttttttttttt")
    print(instance.quantity)
    cart_obj=Cart.objects.filter(entrys=instance).first()
    print(cart_obj)
    if not created:
        total=0
        print("senderrrrrrrrrrrrrrrrrrrrrrrrrrr")

        print(instance)
        print(sender)
        cart_obj=Cart.objects.filter(entrys=instance).first()
        print("cart_obj kooooooooooooooooooooooooooo")
        print(cart_obj)
        if cart_obj is not None:
            if cart_obj.entrys.all().count() > 0:
                for en in cart_obj.entrys.all():
                    print("entry total")
                    print(total)
                    total=total+en.total
                    print("after sum")
                    print(total)

                    print("totalll kossssssssssssssssssssssssssssssss")
                    print(total)

                    cart_obj.subtotal=total
                    cart_obj.save()
m2m_changed.connect(m2m_change_cart_reciver,sender=Cart.entrys.through)
post_save.connect(post_save_cart_reciver,sender=Entry)
def pre_save_cart_receiver(sender,instance,*args,**kwargs):
    if instance.subtotal >0:
        if not instance.has_discount:
            print("not discounttttt")  ##caluculate cart total base on purchced items...NO shipping cost for example
            # instance.total=float(instance.subtotal)*float(1.08)
            instance.total=instance.subtotal
        else:
            discounts=instance.user.discount_set.all()
            print("pree savvee")
            print(discounts.first().status)
            for discount in discounts:
                if discount.status=='اضافه شده':
                    print("اضافه شده")
                    total=instance.total
                    instance.old_total=instance.subtotal
                    disc=discount.percent*instance.subtotal/100
                    instance.total=float(instance.subtotal)-float(disc)
                    print(instance.total)
    else:
        instance.total=0.00

pre_save.connect(pre_save_cart_receiver,sender=Cart)

def delete_signal_entry(sender, instance, using, **kwargs):
    print("deleteeeeeeeeeeeeee")
    dl=instance.cart_set.all()
    print(dl)
post_delete.connect(delete_signal_entry,sender=Entry)