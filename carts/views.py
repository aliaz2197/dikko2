from django.shortcuts import render,redirect
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required
from .models import Cart,Entry
from products.models import Product,Dress
from django.core import serializers
from orders.models import Order
from accounts.models import Profile

# Create your views here.

def cart_detail_api_view(request):
    if request.is_ajax():
        print("ajax here")
    cart_obj, new_obj = Cart.objects.new_or_get(request)



    return JsonResponse({"subtotal":cart_obj.subtotal,"total":cart_obj.total},safe=True)
def cart_home(request):
    cart_obj,new_obj=Cart.objects.new_or_get(request)
    print("hiiiiiiiiiiiiiiiiiiiiiii")
    obj=None
    for entry in cart_obj.entrys.all():
        product_id=entry.product.id
        if Dress.objects.filter(id=product_id).exists(): ### Dress Object
            obj=Dress.objects.get(id=product_id)




    return render(request,"carts/cart.html",{"cart":cart_obj,"cart_entrys":cart_obj.entrys.all(),'obj':obj})

def cart_update(request):
    product_id=request.POST.get('product_id')
    product_color=request.POST.get('product_color')
    product_size=request.POST.get('product_size')
    print("product_id")
    print(product_id)

    entry_id = request.POST.get('entry_id')
    if entry_id is not None: # for remove product in cart view
        print("entry_id")
        print(entry_id)
        entry_obj=Entry.objects.get(id=entry_id)
        pro_obj=entry_obj.product
        if entry_obj.quantity==1:
            cart_obj, new_obj = Cart.objects.new_or_get(request)
            cart_obj.entrys.remove(entry_obj)
            cart_obj.subtotal -=entry_obj.product.price
            cart_obj.save()

        else:
            presave_quantity=entry_obj.quantity-1
            entry_obj.quantity=presave_quantity
            entry_obj.total=presave_quantity*entry_obj.product.price

            entry_obj.save()
        count=0
        cart_obj, new_obj = Cart.objects.new_or_get(request)
        for en in cart_obj.entrys.all():
            count +=en.quantity




        print(request)
        print(count)
        request.session['cart_items_count'] = count
        # request.session['cart_id']=cart_obj.id
        if request.is_ajax():
            # cart_obj, new_obj=Cart.objects.new_or_get(request)
            print("Ajax request")
            json_data={

                "cartItemCount":count
            }
            print(request.session['cart_id'])
            if request.session['cart_id']:
                return JsonResponse(json_data)
        else:
            print("whyyyyyyyyyyy")
            json_data = {

                "cartItemCount": count
            }
            return JsonResponse(json_data)

    if product_id is not None:
        print("gooossssseeeee")
        try:
            product_obj=Product.objects.get(id=product_id)

        except Product.DoesNotExist:
            print("product is gone")
            return redirect('cart:cart_home')
        cart_obj, new_obj = Cart.objects.new_or_get(request)
        entry=None
        sw=False
        if cart_obj.entrys.count()==0:
            entry = Entry()
            entry.product = product_obj
            if product_color is not None:
                entry.color=product_color
            if product_size is not None:
                entry.size=product_size
            print("hi2")
            print(entry.product)
            # entry.cart=cart_obj
            entry.quantity = 1
            entry.total=entry.product.price
            entry.save()
            cart_obj.subtotal = product_obj.price
            cart_obj.entrys.add(entry)

            cart_obj.save()
        else:
            for en in cart_obj.entrys.all():

                if en.product.id==product_obj.id:

                    if (en.color is not None and en.color!=product_color)or(en.size is not None and en.size!=product_size):
                        sw=False
                    else:
                        # cart_obj.products.remove(product_obj)
                        # added=False
                        print("hello")
                        print(product_obj)
                        print(en.pk)
                        # entry=Entry.objects.filter(product__id=product_id).first()
                        # print(entry.pk)
                        # print(entry)
                        # print(entry.product)
                        # print(entry.quantity)
                        if product_color is not None:
                            en.color=product_color
                        if product_size is not None:
                            en.size=product_size
                        en.quantity +=1
                        print(en.quantity)
                        print(en.product.price)
                        print(en.product)
                        en.total=en.quantity*en.product.price
                        en.save()
                        # cart_obj.save()
                        print(en)
                        print("hiii")
                        sw=True


            if sw==False :
                entry = Entry()
                entry.product = product_obj
                print("hi3")
                print(entry.product)
                # entry.cart=cart_obj
                if product_color is not None:
                    entry.color=product_color
                if product_size is not None:
                    entry.size=product_size
                entry.quantity = 1
                entry.total=entry.product.price
                entry.save()
                cart_obj.entrys.add(entry)
                # cart_obj.save()


        quant=0
        for en in cart_obj.entrys.all():
            quant +=en.quantity

        count=quant



        request.session['cart_items_count']=count

        if request.is_ajax():
            print("Ajax request")
            json_data={

                "cartItemCount":count,
                'cart_id':request.session['cart_id']
            }
            return JsonResponse(json_data)
    return redirect("cart:cart_home")

def get_entry(request):
    if request.is_ajax():
        cart_id=request.GET.get('cart_id')
        cart_obj=Cart.objects.get(id=cart_id)
        entrys=cart_obj.entrys.all()
        final=[]
        # serialized_obj={}

        for entry in entrys:
            if entry.product.is_active:

                serialized_obj={'image':str(entry.product.image.all()[0].image),'title':entry.product.title,
                                'url':entry.product.get_absolute_url(),
                                'price':entry.product.price,'quantity':entry.quantity,'total':cart_obj.total}

                final.append(serialized_obj)
            # else:
            #     entry.delete()

        # total={'total':cart_obj.total}
        # final.append(total)
        # obj=serializers.serialize('json', [final, ])
        return JsonResponse(final,safe=False)
