from django.urls import path,include
from . import views

app_name="cart"
urlpatterns=[
    path('',views.cart_home,name='cart_home'),
    path('update/',views.cart_update,name='cart_update'),
    path('api/',views.cart_detail_api_view,name='cart-api'),
    path('get_entry/',views.get_entry,name='get-entry')
]