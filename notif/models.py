from django.db import models
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db.models.signals import pre_save,post_save
# Create your models here.
from django.utils import timezone

from dikko2 import settings
# from .views import serialize_notif
from accounts.models import Profile,Reciever
NOTIFICATION_TARGET = (
    ('1', 'User'),
    ('2', 'EMPLOYEE'),
    ('3', 'BOSS')
)

class Notification(models.Model):
    reciver_s = models.ForeignKey(Reciever,on_delete=models.SET_NULL,blank=True,null=True)
    actor = models.ForeignKey(Profile,on_delete=models.SET_NULL,blank=True,null=True)
    verb = models.CharField(max_length=50)
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE,blank=True,null=True)
    content_object = GenericForeignKey()
    object_id = models.PositiveIntegerField(blank=True,null=True)
    is_read=models.BooleanField(default=False)
    timestamp = models.DateTimeField()
    def save(self, *args, **kwargs):
        ''' On save, update timestamps '''
        if not self.id:
            self.timestamp = timezone.localtime(timezone.now())
        return super(Notification, self).save(*args, **kwargs)

    def __str__(self):
         return f"{self.actor} {self.verb}  at 			{self.timestamp}"

# post_save.connect(serialize_notif,sender=Notification)