from django.urls import path, include, re_path
from . import views

app_name="notif"
urlpatterns=[
   path('api/',views.get_notifications,name='notifications_api'),
   path('read_notif_api/',views.read_notif,name='read_notification_api'),

]