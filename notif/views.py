from django.http import JsonResponse
from django.shortcuts import render
from django.core import serializers
from django.utils import timezone

from .models import Notification
from comments.models import Comment
from orders.models import Order
from accounts.models import Profile
# Create your views here.
# from django.contrib.auth import get_user_model
#
# User = get_user_model()

def serialize_notif(sender,instance,created,*args,**kwargs):
    print(instance)
    serialized_obj = serializers.serialize('json', [instance, ])
    print(serialized_obj)


def get_notifications(request):
    if request.is_ajax():
        if request.user.is_admin:
            print("ajjx")
            print(timezone.localtime(timezone.now()))
            print("end")
            print(request.user.last_login)
            serialize_comments=[]
            serialize_orders=[]
            count=0
            comments=Comment.objects.filter(approved_comment=False).all()
            for comment in comments:
                for notif in comment.comment_notif.all():
                    print(notif)
                    count+=1
                    serialize_comments.append(serializers.serialize('json', [notif, ]))

            orders=Order.objects.filter(status='پرداخت شده').all()
            for order in orders:
                for notif in order.created_notif.all():
                    count+=1
                    serialize_orders.append(serializers.serialize('json', [notif, ]))
            final={'orders':serialize_orders,'comments':serialize_comments,'count':count}
            return JsonResponse(final,safe=False)

        else:
            try:
                count=0;
                profile=Profile.objects.filter(user=request.user).first()
                notifs=profile.Reciever.notification_set.all().filter(is_read=False).all()
                for notif in notifs:
                    count+=1
                if count==0:
                    notif=False
                else:
                    notif=True
                final = { 'count': count,'notif':notif}
                return JsonResponse(final,safe=False)
            except:
                final={'count': 0, 'notif': False}
                return JsonResponse(final, safe=False)

def read_notif(request):
    if request.is_ajax():
        notif_id=request.GET.get('notif_id')

        try:
            notif=Notification.objects.get(id=notif_id)
            notif.is_read=True
            notif.save()
        except:
            pass
        final={'valid':'ok'}
        return JsonResponse(final,safe=False)

