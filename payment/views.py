
import datetime
import requests
from jalali_date import datetime2jalali
from django.shortcuts import render
from orders.models import Order
from .models import Payment
from django.views.decorators.csrf import csrf_exempt
from Cryptodome.PublicKey import RSA
from base64 import b64encode
from django.conf import settings
from . import rsa
from carts.models import Cart
from xml.dom import minidom
# Create your views here.
def home(request):
    cart_obj, cart_created = Cart.objects.new_or_get(request)
    if Order.objects.filter(cart=cart_obj).exists():
        order=Order.objects.get(cart=cart_obj)
        if order.status=='اولیه': ### jologiri az pardakht sefaresh tekrari
            merchantCode=4500268
            terminalCode=1681935
            amount=int(order.total)*10
            callback_uri='https://boghband.com/payment/result/'
            timeStamp=datetime.datetime.now().strftime('%y/%m/%d %H:%M:%S')
            invoiceNumber=order.order_id
            # invoiceDate="2019/02/28 13:11:33"
            indate=datetime.datetime.now()
            invoiceDate=datetime2jalali(indate).strftime('%y/%m/%d %H:%M:%S')
            action="1003"
            data="#"+str(merchantCode)+"#"+str(terminalCode)+"#"+str(invoiceNumber)+"#"+str(invoiceDate)+"#"+str(amount)+"#"+str(callback_uri)+"#"+str(action)+"#"+str(timeStamp)+"#"
            dir=settings.BASE_DIR+"/payment/private2.pem"
            key=open(dir, "r").read()
            private=RSA.importKey(key)

            signature=b64encode(rsa.sign(bytes(data, 'utf-8'), private, "SHA-1"))
            sign=signature.decode("utf-8")
            order.invoiceDate=indate
            order.save()

            # if request.POST:
            #     print(request.POST)
            #     payMethod=request.POST.get('payMethod')
            #     if payMethod=='1':## online payment choose
            #         r=requests.post('http://127.0.0.1:8000/payment/result/' ,data={'kir':5})
            #         print(r.status_code)
                    # wsdl='http://api.nextpay.org/gateway/token.wsdl'
                    # client=zeep.Client(wsdl=wsdl)
                    # amount=int(order.total)
                    # api_key='7564be43-5660-4d79-9192-ae6358a5a0b5'
                    # token_obj=client.service.TokenGenerator(api_key=api_key,
                    #                                         order_id=order.order_id,
                    #                                         amount=amount,
                    #                                         callback_uri='http://127.0.0.1:8000/payment/result/')
                    # trans_id=token_obj.trans_id
                    # code=token_obj.code
                    # payment=Payment(order=order,trans_id=trans_id,code=code)
                    # payment.save()
                    # if code == -1:
                    #     url='http://api.nextpay.org/gateway/payment/'+trans_id
                    #     return redirect(url)


    #     print(order)
    #
    #     wsdl='http://api.nextpay.org/gateway/token.wsdl'
    #     client=zeep.Client(wsdl=wsdl)
    #     amount=int(order.total)
    #     print(amount)
    #     api_key='7564be43-5660-4d79-9192-ae6358a5a0b5'
    #     params={'api_key':api_key,'order_id':'NS4JINQ87F','amount':22000,'callback_uri':'http://127.0.0.1:8000/payment/callback/'}
    #     print("sdsdscsdc")
    #     token_obj=client.service.TokenGenerator(api_key='7564be43-5660-4d79-9192-ae6358a5a0b5',
    #                                         order_id=order.order_id,
    #                                         amount=amount,
    #                                         callback_uri='http://127.0.0.1:8000/payment/callback/')
    #     print(token_obj)
    #     trans_id=token_obj.trans_id
    #     code=token_obj.code
    #     print(trans_id)
    #     print(code)
    #     if code == -1:
    #         url='http://api.nextpay.org/gateway/payment/'+trans_id
    #         return redirect(url)


            return render(request,"payment/payment.html",{"cart_obj":cart_obj,"order":order,"merchantCode":merchantCode
                                                          ,"terminalCode":terminalCode,"invoiceNumber":invoiceNumber,
                                                          "invoiceDate":invoiceDate,"amount":amount,
                                                          "redirectAddress":callback_uri,"action":action,"timeStamp":timeStamp,
                                                          "sign":sign})
    # else:
    #     print("invalid")
@csrf_exempt
def result(request):

    # return render(request, 'payment/result.html', {"valid": 1})
    InvoiceNumber=request.GET.get('iN')
    InvoiceDate=request.GET.get('iD')
    TransactionReferenceID=request.GET.get('tref')
    url='https://pep.shaparak.ir/CheckTransactionResult.aspx'

    postdata={
        'invoiceUID':TransactionReferenceID,
    }

    r=requests.post(url, data=postdata)
    print(r.status_code)
    print(r.text)
    xmldoc=minidom.parseString(r.text)
    amount=xmldoc.getElementsByTagName('amount')[0].firstChild.data
    order_id=xmldoc.getElementsByTagName('invoiceNumber')[0].firstChild.data
    merchantCode=xmldoc.getElementsByTagName('merchantCode')[0].firstChild.data
    terminalCode=xmldoc.getElementsByTagName('terminalCode')[0].firstChild.data
    trans_id=xmldoc.getElementsByTagName('transactionReferenceID')[0].firstChild.data
    result=xmldoc.getElementsByTagName('result')[0].firstChild.data
    action=xmldoc.getElementsByTagName('action')[0].firstChild.data
    trace_num=xmldoc.getElementsByTagName('traceNumber')[0].firstChild.data
    reference_num=xmldoc.getElementsByTagName('referenceNumber')[0].firstChild.data
    # for a in amount:
    print(amount)
    print(merchantCode)
    print(order_id)
    print(terminalCode)
    if result=='True':
        if trans_id==TransactionReferenceID and order_id==InvoiceNumber:
            if Order.objects.filter(order_id=order_id).exists():
                order_obj=Order.objects.get(order_id=order_id)
                if order_obj.status== 'اولیه':
                    indate=order_obj.invoiceDate
                    invoiceDate=datetime2jalali(indate).strftime('%y/%m/%d %H:%M:%S')
                    timeStamp=datetime.datetime.now().strftime('%y/%m/%d %H:%M:%S')
                    data="#"+str(merchantCode)+"#"+str(terminalCode)+"#"+str(InvoiceNumber)+"#"\
                         +str(invoiceDate)+"#"+str(amount)+"#"+str(timeStamp)+"#"
                    dir=settings.BASE_DIR+"/payment/private2.pem"
                    key=open(dir, "r").read()
                    private=RSA.importKey(key)

                    signature=b64encode(rsa.sign(bytes(data, 'utf-8'), private, "SHA-1"))
                    sign=signature.decode("utf-8")
                    url='https://pep.shaparak.ir/VerifyPayment.aspx'
                    # ... your code ...

                    postdata={
                        'InvoiceNumber': InvoiceNumber,
                        'InvoiceDate':InvoiceDate,
                        'MerchantCode':merchantCode,
                        'TerminalCode':terminalCode,
                        'Amount':amount,
                        'TimeStamp':timeStamp,
                        'Sign':sign

                    }

                    r=requests.post(url, data=postdata)
                    print(r.status_code)
                    print(r.text)
                    xmldoc=minidom.parseString(r.text)
                    result=xmldoc.getElementsByTagName('result')[0].firstChild.data
                    print(result)
                    if result=='True':
                        order_obj.status='پرداخت شده'
                        order_obj.save()
                        payment=Payment(trans_id=trans_id,code=0,order=order_obj,action=action,
                                        trace_num=trace_num,reference_num=reference_num)
                        payment.save()


                        cart_obj=Cart.objects.new(request.user)
                        request.session['cart_id']=cart_obj.id
                        request.session['cart_items_count']=0
                        return render(request, 'payment/result.html', {"valid": 1, "order": order_obj})
                    else:
                        return render(request, 'payment/result.html', {"valid": 0, "order": order_obj})

    else:
        if Order.objects.filter(order_id=order_id).exists():
            order_obj=Order.objects.get(order_id=order_id)
            print("Not successfull")
            return render(request, 'payment/result.html', {"valid": 0,"order": order_obj})




    #     order_id=request.POST.get('order_id')
    #     trans_id=request.POST.get('trans_id')
    #     if Order.objects.filter(order_id=order_id).exists() and Payment.objects.filter(trans_id=trans_id).exists():
    #         order_obj=Order.objects.get(order_id=order_id)
    #         payment_obj=Payment.objects.get(trans_id=trans_id)
    #         if order_obj.status=='اولیه':
    #             wsdl='http://api.nextpay.org/gateway/token.wsdl'
    #             client=zeep.Client(wsdl=wsdl)
    #             amount=int(order_obj.total)
    #             token_obj=client.service.PaymentVerification(api_key='7564be43-5660-4d79-9192-ae6358a5a0b5',
    #                                                          order_id=order_id,
    #                                                          amount=amount,
    #                                                          trans_id=trans_id)
    #             if token_obj.code==0:
    #                 order_obj.status='پرداخت شده'
    #                 order_obj.save()
    #                 payment_obj.code=token_obj.code
    #                 payment_obj.save()
    #                 return render(request, 'payment/result.html', {"valid":1,"order":order_obj})
    #
    #             else:
    #                 payment_obj.code=token_obj.code
    #                 payment_obj.save()
    #                 return render(request, 'payment/result.html', {"valid":0,"order":order_obj})

