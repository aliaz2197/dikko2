from django.urls import path
from .views import home,result
app_name="payment"
urlpatterns=[
    path('',home,name='home'),
    path('result/',result,name='result')
]