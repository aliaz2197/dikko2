from django.db import models
from django.utils import timezone

from orders.models import Order
# Create your models here.

class Payment(models.Model):
    order=models.ForeignKey(Order,on_delete=models.SET_NULL,null=True)
    trans_id=models.CharField(max_length=50)
    trace_num=models.CharField(max_length=15,null=True)
    reference_num=models.CharField(max_length=15,null=True)
    action=models.CharField(max_length=10,null=True)
    code=models.IntegerField()
    created_date=models.DateTimeField()
    modified_date=models.DateTimeField()

    def __str__(self):
        return self.order.order_id
    def save(self, *args, **kwargs):
        ''' On save, update timestamps '''
        if not self.id:
            self.created_date = timezone.localtime(timezone.now())
        self.modified_date=timezone.localtime(timezone.now())

        return super(Payment, self).save(*args, **kwargs)
