import datetime
from xml.dom import minidom

import requests
from django.http import JsonResponse, Http404, HttpResponseRedirect
from django.shortcuts import render, redirect
from django.urls import reverse
from django.views.generic import TemplateView, UpdateView, ListView, CreateView, DetailView, DeleteView
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.contrib.auth.decorators import login_required
from jalali_date import datetime2jalali

from products.models import Product, Dress, CImage, get_upload_path, Zivar,RuDuzi,Dast_Saze
# from django.contrib.auth.models import User
from django.views.generic.edit import FormMixin
from comments.models import Comment
from accounts.models import User
from .forms import info_form, info_form_user, info_dikko_form, add_product, create_dikko, \
    address_form, create_notif, add_dress, add_zivar, add_ruDuzi,add_DastSaze_Honari
from accounts.models import Profile, Seller_Profile, City, Address, Reciever
from dikko.models import Dikko, Sub_Field, Field
from notif.models import Notification
from orders.models import Order
from django.conf import settings
import os
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from dikko2.utils import unique_product_code_generator
from .email import send_email


# User = get_user_model()
# Create your views here.
class AdminStaffRequiredMixin(LoginRequiredMixin, UserPassesTestMixin):

    def test_func(self):
        return self.request.user.is_admin


@login_required
def dashboard_account_information(request):
    instance=Profile.objects.get(user=request.user)
    user=User.objects.get(email=request.user.email)
    user_form=info_form_user(request.POST or None, instance=user)
    form=info_form(request.POST or None, instance=instance)
    addre_form=address_form()
    address=instance.address_set.all()
    print(address)
    success=False
    if request.method=='POST':
        print("helooo")
        if form.is_valid():
            form.save()
        if user_form.is_valid():
            user_form.save()
            success=True
        else:
            print(form.errors)
            print(user_form.errors)
    return render(request, 'dashboard-personalInformation.html',
                  {'form': form, 'profile': instance, 'user_form': user_form, 'address': address, 'success': success,
                   'address_form': addre_form})


class Dikko_Info_List(AdminStaffRequiredMixin, ListView):
    template_name='dashboard-dikko-info.html'
    model=Dikko
    queryset=Dikko.objects.all()
    paginate_by=8

    def get_context_data(self, **kwargs):
        context=super().get_context_data(**kwargs)
        profile=Profile.objects.get(user=self.request.user)
        context['profile']=profile
        return context


class Dikko_Products_List(AdminStaffRequiredMixin, ListView):
    template_name='dashboard-dikko-products-list.html'
    paginate_by=8

    def get_queryset(self):
        dikko_pk=self.kwargs.get('pk')
        dikko=Dikko.objects.get(pk=dikko_pk)
        return dikko.products.all()

    def get_context_data(self, **kwargs):
        context=super().get_context_data(**kwargs)
        profile=Profile.objects.get(user=self.request.user)
        context['profile']=profile
        dikko_pk=self.kwargs.get('pk')
        dikko=Dikko.objects.get(pk=dikko_pk)
        context['dikko']=dikko
        return context


class Dikko_Info_Details(AdminStaffRequiredMixin, UpdateView):
    template_name='dashboard-dikko-info-details.html'
    model=Dikko
    form_class=info_dikko_form

    def get_context_data(self, **kwargs):
        context=super().get_context_data(**kwargs)
        profile=Profile.objects.get(user=self.request.user)
        context['profile']=profile
        return context

    def get_success_url(self):
        return reverse('dashboard:dashboard_dikko_info')


class Dikko_Add_Product(AdminStaffRequiredMixin, DetailView):
    model=Product
    template_name='dashboard-dikko-management-add-product.html'

    # form_class = add_product
    def get_context_data(self, **kwargs):
        context=super().get_context_data(**kwargs)
        profile=Profile.objects.get(user=self.request.user)
        context['profile']=profile
        dikko=self.get_object()
        sfileds=dikko.sfield.all()
        dress=False
        zivar=False
        for sfield in sfileds:
            if sfield.scode[0]=='D':
                if sfield.scode[1]=='H':
                    context['another_form']=add_DastSaze_Honari()
                else:
                    dress_form=add_dress()
                    context['another_form']=dress_form

            elif sfield.scode[0]=='H':
                if sfield.scode[1]=='J':  ### zivarAlat
                    zivar_form=add_zivar()
                    context['another_form']=zivar_form
                elif sfield.scode[1]=='R':  # RuDUzi
                    ruDuzi=add_ruDuzi()
                    context['another_form']=ruDuzi
            elif sfield.scode[0]=='A':  # Agricuture
                pass

        return context

    def post(self, request, *args, **kwargs):
        self.object=self.get_object()

        # another_form=add_dress(self.request.POST,prefix='another_form')
        print("HIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII")
        dikko=self.get_object()
        sfileds=dikko.sfield.all()
        another_form=None
        for sfield in sfileds:
            if sfield.scode[0]=='D':
                if sfield.scode[1]=='H':
                    another_form=add_DastSaze_Honari(self.request.POST)
                else:
                    another_form=add_dress(self.request.POST)
                    print("ANOTHERRRRRRRRRRRRR")
                    print(another_form)
            elif sfield.scode[0]=='H':
                if sfield.scode[1]=='J':  ### zivarAlat
                    another_form=add_zivar(self.request.POST)
                elif sfield.scode[1]=='R':
                    another_form=add_ruDuzi(self.request.POST)
        # print(self.form_class)
        # print(self.get_form_class())
        # print(self.get_form())
        dirname=self.object.name
        dir=settings.MEDIA_ROOT
        print(dir)
        product_dir=os.path.join(dir, 'products')
        image_dir=os.path.join(product_dir, 'img')
        seller_dir=os.path.join(image_dir, dirname)
        if not os.path.isdir(seller_dir):
            os.mkdir(seller_dir)
        print(self.request.POST)
        print(another_form.errors)
        if another_form.is_valid():

            kwargs={'another_form': another_form}
            return self.form_valid(another_form)

        else:
            print("NOTTTT VALID")
            return self.render_to_response(self.get_context_data())

    def form_valid(self, form):

        new_p=form.save()
        profile=Profile.objects.filter(seller_profile=self.get_object().owner).first()
        print(self.request.FILES)
        new_p.seller=profile
        new_p.dikko_name=self.get_object().name
        new_p.dikko_slug=self.get_object().slug
        new_p.code=unique_product_code_generator(new_p, self.get_object())

        new_p.save()
        # form.save()
        new_p.save()
        for afile in self.request.FILES.getlist('files'):
            image=afile

            path='products/img/{0}/{1}/{2}'.format(new_p.dikko_name, new_p.title, image)
            ci=CImage()
            ci.path=path
            ci.save()

            ci.image=image
            ci.save()
            new_p.image.add(ci)
            new_p.save()

        if isinstance(new_p, Dress):
            print(new_p.color.all())
            print(new_p.price)

        self.get_object().products.add(new_p)
        return redirect('dashboard:dashboard_dikko_pre_add')

    def get_object(self, *args, **kwargs):
        request=self.request
        pk=self.kwargs.get('pk')
        try:
            instance=Dikko.objects.get(pk=pk)
            print(instance.name)
        except Product.DoesNotExist:
            raise Http404("Not Found..")

        return instance

    def get_success_url(self):
        pk=self.kwargs.get('pk')
        return reverse('dashboard:dashboard_dikko_management_add_product', kwargs={'pk': pk})


class Dikko_Remove_Product(AdminStaffRequiredMixin, DeleteView):
    model=Product
    template_name='dashboard-delete-product.html'

    def get_object(self, queryset=None):
        request=self.request
        pk=self.kwargs.get('pk')
        pk2=self.kwargs.get('pro')
        try:
            pro=Product.objects.get(pk=pk2)
            instance=Dikko.objects.get(pk=pk)
            print(instance.name)
        except Product.DoesNotExist:
            raise Http404("Not Found..")

        return pro

    def get_success_url(self):
        pk=self.kwargs.get('pk')
        return reverse('dashboard:dashboard_dikko_products_list_remove', kwargs={'pk': pk})


@login_required
def Dikko_Edit_Product(request, pk, pro):
    dikko=Dikko.objects.get(pk=pk)
    sfileds=dikko.sfield.all()
    form=None
    for sfield in sfileds:
        if sfield.scode[0]=='D':
            if sfield.scode[1]=='H':
                dast_saze=Dast_Saze.objects.get(pk=pro)
                form=add_DastSaze_Honari(request.POST or None,instance=dast_saze)
            else:
                dress=Dress.objects.get(pk=pro)
                form=add_dress(request.POST or None, instance=dress)
        elif sfield.scode[0]=='H':
            if sfield.scode[1]=='J':
                zivar=Zivar.objects.get(pk=pro)
                form=add_zivar(request.POST or None, instance=zivar)
            elif sfield.scode[1]=='R':
                ruduzi=RuDuzi.objects.get(pk=pro)
                form=add_ruDuzi(request.POST or None , instance=ruduzi)

    if request.POST:
        if form.is_valid():
            new_p=form.save(commit=False)
            new_p.code=unique_product_code_generator(new_p, dikko)
            new_p.save()
            form.save_m2m()
            new_p.save()
            for afile in request.FILES.getlist('files'):
                image=afile

                path='products/img/{0}/{1}/{2}'.format(new_p.dikko_name, new_p.title, image)
                ci=CImage()
                ci.path=path
                ci.save()

                ci.image=image
                ci.save()
                new_p.image.add(ci)
                new_p.save()
            form.save()
            print(pk)
            return HttpResponseRedirect(reverse('dashboard:dashboard_dikko_products_list_edit', kwargs={'pk': pk}))
        else:
            print(form.errors)
            return render(request, 'dashboard-edit-product.html', {'another_form': form})

    return render(request, 'dashboard-edit-product.html', {'another_form': form})

    # model = Product
    # template_name = 'dashboard-edit-product.html'
    # # form_class = add_product
    #
    # def get_object(self, queryset=None):
    #     request = self.request
    #     pk = self.kwargs.get('pk')
    #     pk2 = self.kwargs.get('pro')
    #     try:
    #         pro = Product.objects.get(pk=pk2)
    #         instance = Dikko.objects.get(pk=pk)
    #         print(instance.name)
    #     except Product.DoesNotExist:
    #         raise Http404("Not Found..")
    #
    #     return pro
    # def get_context_data(self, **kwargs):
    #     context = super().get_context_data(**kwargs)
    #     profile = Profile.objects.get(user=self.request.user)
    #     context['profile'] = profile
    #     return context
    #
    #
    # def get_success_url(self):
    #     pk = self.kwargs.get('pk')
    #     return reverse('dashboard:dashboard_dikko_products_list_edit', kwargs={'pk': pk})


class AjaxableResponseMixin:
    """
    Mixin to add AJAX support to a form.
    Must be used with an object-based FormView (e.g. CreateView)
    """

    def form_invalid(self, form):
        response=super().form_invalid(form)
        if self.request.is_ajax():
            return JsonResponse(form.errors, status=400)
        else:
            return response

    def form_valid(self, form):
        # We make sure to call the parent's form_valid() method because
        # it might do some processing (in the case of CreateView, it will
        # call form.save() for example).
        response=super().form_valid(form)
        if self.request.is_ajax():
            data={
                'pk': self.object.pk,
            }
            return JsonResponse(data)
        else:
            return response


class Create_dikko(AdminStaffRequiredMixin, AjaxableResponseMixin, CreateView):
    form_class=create_dikko
    template_name='dashboard-create-dikko.html'

    def get_context_data(self, **kwargs):
        context=super().get_context_data(**kwargs)
        profile=Profile.objects.get(user=self.request.user)
        context['profile']=profile
        return context

    def get_success_url(self):
        return reverse('dashboard:dashboard_create_dikko')

    def form_valid(self, form):
        owner=Seller_Profile.objects.get(user=self.request.user)
        dikko=form.save(commit=False)
        image=form.cleaned_data['image']
        print("hello")
        print(image)
        dikko.image=image
        dikko.owner=owner

        return super(Create_dikko, self).form_valid(form)


class dashboard_dikko_management(AdminStaffRequiredMixin, TemplateView):
    template_name='dashboard-dikko-management.html'


class dashboard_orders(LoginRequiredMixin, ListView):
    template_name='dashboard-orders.html'
    model=Order
    context_object_name='orders'

    def get_context_data(self, *, object_list=None, **kwargs):
        context=super().get_context_data(**kwargs)
        pro=Profile.objects.filter(user=self.request.user).first()
        context['profile']=pro
        return context

    def get_queryset(self):
        pro=Profile.objects.filter(user=self.request.user).first()
        return Order.objects.filter(user=pro).order_by('-modified_date')


@login_required
def address_create(request):
    form=address_form(request.POST or None)
    profile=Profile.objects.get(user=request.user)
    if request.POST:
        if form.is_valid():
            owner=Profile.objects.get(user=request.user)
            address=form.save(commit=False)
            address.profile=owner
            address.save()
            return HttpResponseRedirect(reverse("dashboard:dashboard_addresses"))
        else:
            return render(request, 'dashboard-create-address.html', {"form": form, "profile": profile})
    else:
        return render(request, 'dashboard-create-address.html', {"form": form, "profile": profile})


# class Address_Create(LoginRequiredMixin,CreateView):
#     form_class = address_form
#
#
#     def get_success_url(self):
#         x=self.request.META.get('HTTPS_REFERER')
#         return x
#
#     def form_valid(self, form):
#         owner = Profile.objects.get(user=self.request.user)
#         address = form.save(commit=False)
#         print("dfvndkvndfvn")
#         print(self.request.POST)
#         print(address)
#         address.profile = owner
#
#         return super(Address_Create, self).form_valid(form)
class Address_Delete(LoginRequiredMixin, DeleteView):
    model=Address
    template_name='dashboard-delete-address.html'

    def get_object(self, queryset=None):
        request=self.request
        pk=self.kwargs.get('pk')
        try:

            instance=Address.objects.get(pk=pk)

        except Product.DoesNotExist:
            raise Http404("Not Found..")

        return instance

    def get_success_url(self):
        return reverse('dashboard:dashboard_addresses')


class Address_Update(LoginRequiredMixin, UpdateView):
    model=Address
    form_class=address_form
    template_name='dashboard-edit-address.html'

    def get_context_data(self, *, object_list=None, **kwargs):
        context=super().get_context_data(**kwargs)
        pro=Profile.objects.filter(user=self.request.user).first()
        if self.kwargs['return']=='shipping':
            context['scroll']=True
        context['profile']=pro

        return context

    def form_valid(self, form):
        owner=Profile.objects.get(user=self.request.user)
        address=form.save(commit=False)

        address.profile=owner

        return super(Address_Update, self).form_valid(form)

    def get_success_url(self):
        if self.kwargs['return']=='shipping':
            return reverse('shipping:home')
        return reverse('dashboard:dashboard_addresses')


class Address_List(LoginRequiredMixin, ListView):
    template_name='dashboard-addresses.html'

    def get_queryset(self):
        user=self.request.user
        profile=Profile.objects.get(user=user)
        return profile.address_set.all()

    def get_context_data(self, *, object_list=None, **kwargs):
        context=super().get_context_data(**kwargs)
        pro=Profile.objects.filter(user=self.request.user).first()
        form=address_form()
        context['profile']=pro
        context['form']=form
        return context


def load_cities(request):
    state_id=request.GET.get('state')
    cities=City.objects.filter(state_id=state_id).order_by('name')
    return render(request, 'city_dropdown_list_options.html', {'cities': cities})


def load_fields(request):
    field_ids=request.GET.getlist('field[]')
    sfield=[]
    for field_id in field_ids:
        x=Sub_Field.objects.filter(field_id=field_id).order_by('name')
        if x is not None:
            sfield.append(x)
    return render(request, 'fields-select-list.html', {'sfields': sfield})


@login_required
def notifications_orders_list(request):
    profile=Profile.objects.get(user=request.user)
    if request.user.is_admin:
        notifications=[]
        orders=Order.objects.exclude(status='اولیه').order_by('-modified_date')
        page=request.GET.get('page', 1)
        for order in orders:
            notifs=order.created_notif.all()
            for notif in notifs:
                notifications.append(notif)
        paginator=Paginator(notifications, 7)
        try:
            notifications=paginator.page(page)
        except PageNotAnInteger:
            notifications=paginator.page(1)
        except EmptyPage:
            notifications=paginator.page(paginator.num_pages)
        return render(request, 'dashboard-notifications-orders.html',
                      {'notifications': notifications, 'profile': profile})


@login_required
def notifications_comments_list(request):
    profile=Profile.objects.get(user=request.user)
    if request.method=='POST':

        if request.user.is_admin:

            comment_id=request.POST.get('commentId')
            comment_obj=Comment.objects.get(id=comment_id)
            if request.POST.get('deleteComment')=='True':
                comment_obj.delete()
            else:

                print(comment_obj)
                comment_obj.approve()
                print(comment_obj)
            for notif in comment_obj.comment_notif.all():
                notif.is_read=True
                notif.save()
                print(notif)

    if request.user.is_admin:
        page=request.GET.get('page', 1)
        notifications=[]
        comments=Comment.objects.all()
        for comment in comments:
            notifs=comment.comment_notif.all()
            for notif in notifs:
                notifications.append(notif)

        paginator=Paginator(notifications, 7)

        try:
            notifications=paginator.page(page)
        except PageNotAnInteger:
            notifications=paginator.page(1)
        except EmptyPage:
            notifications=paginator.page(paginator.num_pages)

        return render(request, 'dashboard-notifications-comments.html',
                      {'notifications': notifications, 'profile': profile})


class notifications_orders_details(AdminStaffRequiredMixin, DetailView):
    model=Order
    # form_class = add_product
    template_name='dashboard-notifications-orders-details.html'

    def get_context_data(self, **kwargs):
        context=super().get_context_data(**kwargs)
        profile=Profile.objects.get(user=self.request.user)
        context['profile']=profile
        # order=self.get_object()
        # context['subProd']=''
        # for entry in order.cart.entrys.all():
        #     dikko=Dikko.objects.get(name=entry.product.dikko_name)
        #     for sfield in dikko.sfield.all():
        #         if sfield.scode=='DS':
        #             print("DS")
        #             dress=Dress.objects.get(id=entry.product.id)
        #
        #             context['subProd']=dress
        #             # dress=
        #         elif sfield.scode[0]=='H':
        #             if sfield.scode[1]=='J':  ### zivarAlat
        #                 zivar=Zivar.objects.get(id=entry.product.id)
        #                 context['subProd']=zivar
        #         elif sfield.scode[0]=='A':  # Agricuture
        #             pass
        return context

    def post(self, request, *args, **kwargs):
        if request.user.is_admin:
            self.object=self.get_object()
            user=self.get_object().user.user
            if request.POST.get('rejectOrder') is not None:  # reject Order

                merchantCode=4500268
                terminalCode=1681935
                amount=int(self.object.total) * 10
                invoiceNumber=self.object.order_id
                invoiceDate=datetime2jalali(self.object.invoiceDate).strftime('%y/%m/%d %H:%M:%S')
                action="1004"
                timeStamp=datetime.datetime.now().strftime('%y/%m/%d %H:%M:%S')
                print(timeStamp)
                from base64 import b64encode
                from payment import rsa
                from Cryptodome.PublicKey import RSA
                data="#"+str(merchantCode)+"#"+str(terminalCode)+"#"+str(invoiceNumber)+"#"+str(invoiceDate)+"#"+str(
                    amount)+"#"+str(action)+"#"+str(timeStamp)+"#"
                dir=settings.BASE_DIR+"/payment/private2.pem"
                key=open(dir, "r").read()
                private=RSA.importKey(key)

                signature=b64encode(rsa.sign(bytes(data, 'utf-8'), private, "SHA-1"))
                sign=signature.decode("utf-8")
                url='https://pep.shaparak.ir/DoRefund.aspx'

                postdata={
                    'InvoiceNumber': invoiceNumber,
                    'InvoiceDate': invoiceDate,
                    'MerchantCode': merchantCode,
                    'TerminalCode': terminalCode,
                    'Amount': amount,
                    'action': action,
                    'TimeStamp': timeStamp,
                    'Sign': sign

                }

                r=requests.post(url, data=postdata)
                print(r.status_code)
                print(r.text)
                xmldoc=minidom.parseString(r.text)
                result=xmldoc.getElementsByTagName('result')[0].firstChild.data
                print(result)
                if result=='True':
                    self.object.status='رد شده'
                    self.object.save()

                    subject='رد سفارش توسط بق بند'
                    message='با عرض پوزش متاسفانه سفارش شما به دلیل تمام شدن موجودی قابل ارسال نمی باشد. وجه شما طی 48 ساعت آینده به حساب شما برگشت' \
                            'داده میشود.' \
                            'با تشکر بق بند'

                    send_email(subject, message, user)

            elif request.POST.get('tracePost') is not None:
                tracePost=request.POST.get('tracePost')
                self.object.status='ارسال شده'
                self.object.tracePost=tracePost
                self.object.save()
                subject='کد رهگیری سفارش'
                message='سفارش شما به اداره پست تحویل داده شد. کد رهگیری:\n' \
                        +tracePost+' با تشکر از خرید شما '
                send_email(subject, message, user)
            else:  # accept order
                self.object.status='تایید شده'
                self.object.save()
                for entry in self.object.cart.entrys.all():
                    entry.product.available_quantity=entry.product.available_quantity-entry.quantity
                    entry.product.purchaced=entry.product.purchaced+entry.quantity
                    entry.product.save()
                subject='تایید سفارش توسط بق بند'
                message='تبریک سفارش شما تایید شد!.پس از ارسال سفارش توسط پست کد رهگیری به شما اطلاع داده میشود.' \
                        'با تشکر بق بند'
                send_email(subject, message, user)
            for notif in self.object.created_notif.all():
                notif.is_read=True
                notif.save()

            return HttpResponseRedirect(self.request.path_info)

    def get_object(self, *args, **kwargs):
        request=self.request
        pk=self.kwargs.get('pk')
        try:
            instance=Order.objects.get(pk=pk)
        except Product.DoesNotExist:
            raise Http404("Not Found..")

        return instance

    # def get_success_url(self):
    #     pk = self.kwargs.get('pk')
    #     return reverse('dashboard:dashboard_dikko_management_add_product', kwargs={'pk': pk})


@login_required
def create_notification(request):
    form=create_notif()
    if request.method=='POST' and request.user.admin:
        form=create_notif(data=request.POST)
        print(form.errors)
        if form.is_valid():
            text=form.cleaned_data.get('verb')
            recievers=Reciever()
            recievers.broadcast=True
            recievers.save()
            # admin=User.objects.get(admin=True)
            admin_pro=Profile.objects.get(user=request.user)
            profiles=Profile.objects.all()
            for profile in profiles:
                profile.Reciever=recievers
                profile.save()

            notif=Notification(reciver_s=recievers, actor=admin_pro, verb=text)
            notif.save()

    return render(request, 'dashboard-create-notification.html', {"form": form})


class Notifications_List(LoginRequiredMixin, ListView):
    template_name='dashboard-notifications-list.html'
    paginate_by=10

    def get_context_data(self, **kwargs):
        context=super().get_context_data(**kwargs)
        profile=Profile.objects.get(user=self.request.user)
        context['profile']=profile
        return context

    def get_queryset(self):
        profile=Profile.objects.get(user=self.request.user)
        # profile=self.request.user.pro
        try:

            notifis=profile.Reciever.notification_set.all()
        except:
            notifis=[]
        return notifis
