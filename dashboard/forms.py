from django import forms
from accounts.models import Profile,Seller_Profile,User,City,Address
from dikko.models import Dikko,Field,Sub_Field
from products.models import Product,Dress,Zivar,RuDuzi,Dast_Saze
from tinymce import TinyMCE
from notif.models import Notification
class info_form_user(forms.ModelForm):
    class Meta:
        model=User
        fields=('email','first_name','last_name')
        widgets={
            'email':forms.TextInput(attrs={'class': 'form-control','style': 'font-size: medium','readonly':'readonly'}),
            'first_name': forms.TextInput(attrs={'class': 'form-control','style': 'font-size: medium','readonly':'readonly'}),
            'last_name': forms.TextInput(attrs={'class': 'form-control','style': 'font-size: medium','readonly':'readonly'})
        }

class address_form(forms.ModelForm):
    class Meta:
        model=Address
        fields='__all__'
        widgets = {

            'address': forms.Textarea(attrs={'class': 'form-control','style': 'font-size: medium'}),
            'receiver_name': forms.TextInput(attrs={'class': 'form-control','style': 'font-size: medium'}),
            'receiver_phone_number': forms.TextInput(attrs={'class': 'form-control','style': 'font-size: medium'}),
            'state': forms.TextInput(attrs={'class': 'form-control','style': 'font-size: medium',}),
            'city': forms.TextInput(attrs={'class': 'form-control','style': 'font-size: medium'}),
            'postal_code':forms.TextInput(attrs={'class': 'city form-control','style': 'font-size: medium'}),
        }
    # def __init__(self, *args, **kwargs):
    #     super().__init__(*args, **kwargs)
    #     self.fields['city'].queryset = City.objects.none()
    #
    #     if 'state' in self.data:
    #         try:
    #             state_id = int(self.data.get('state'))
    #             self.fields['city'].queryset = City.objects.filter(state_id=state_id).order_by('name')
    #         except (ValueError, TypeError):
    #             pass  # invalid input from the client; ignore and fallback to empty City queryset
    #     elif self.instance.pk:
    #         if self.instance.state is not None:
    #             self.fields['city'].queryset = self.instance.state.city_set.order_by('name')

class info_form(forms.ModelForm):
    class Meta:
        model=Profile
        fields=('national_code','phone_number','state','city')
        widgets={

            'national_code': forms.TextInput(attrs={'class': 'form-control','style': 'font-size: medium'}),
            'phone_number': forms.TextInput(attrs={'class': 'form-control','style': 'font-size: medium'}),
            'state':forms.TextInput(attrs={'class': 'form-control','style': 'font-size: medium'}),
            'city':forms.TextInput(attrs={'class': 'form-control','style': 'font-size: medium'}),
        }

    # def __init__(self, *args, **kwargs):
    #     super().__init__(*args, **kwargs)
    #     self.fields['city'].queryset = City.objects.none()
    #
    #     if 'state' in self.data:
    #         try:
    #             state_id = int(self.data.get('state'))
    #             self.fields['city'].queryset = City.objects.filter(state_id=state_id).order_by('name')
    #         except (ValueError, TypeError):
    #             pass  # invalid input from the client; ignore and fallback to empty City queryset
    #     elif self.instance.pk:
    #         if self.instance.state is not None:
    #             self.fields['city'].queryset = self.instance.state.city_set.order_by('name')


class info_dikko_form(forms.ModelForm):

    class Meta:
        model=Dikko
        fields=('name','url','image','field','sfield')
        widgets={
               'name':forms.TextInput(attrs={'class':'form-control'}),
            'url':forms.TextInput(attrs={'class':'form-control'}),
            'field':forms.SelectMultiple(attrs={'class':'form-control'}),
            'sfield': forms.SelectMultiple(attrs={'class': 'form-control'})

        }


    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


        if 'field' in self.data:
            try:
                self.fields['sfield'].queryset = Field.objects.none()
                field_id = int(self.data.get('field'))
                self.fields['sfield'].queryset = Sub_Field.objects.filter(field_id=field_id).order_by('name')
            except (ValueError, TypeError):
                pass


class create_dikko(forms.ModelForm):
    class Meta:
        model=Dikko
        fields = ('name', 'description', 'field','sfield', 'image','image2')
        widgets={
            'name':forms.TextInput(attrs={'class':'form-control'}),
            'description':forms.Textarea(attrs={'class':'form-control'}),
            'field':forms.SelectMultiple(attrs={'class':'form-control'}),
            'sfield': forms.SelectMultiple(attrs={'class': 'form-control'})

        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


        if 'field' in self.data:
            try:
                self.fields['sfield'].queryset = Field.objects.none()
                field_id = int(self.data.get('field'))
                self.fields['sfield'].queryset = Sub_Field.objects.filter(field_id=field_id).order_by('name')
            except (ValueError, TypeError):
                pass
class TinyMCEWidget(TinyMCE):
    def use_required_attribute(self, *args):
        return False




class add_product(forms.ModelForm):
    description = forms.CharField(
        widget=TinyMCEWidget(
            attrs={'required': False, 'cols': 30, 'rows': 10,'plugins':'directionality'}
        )
    )
    class Meta:
        model=Product
        exclude=("seller",)
        widgets={
            'title':forms.TextInput(attrs={'class':'form-control'}),
            # 'description':forms.TextInput()
        }

class add_dress(forms.ModelForm):
    class Meta:
        model=Dress
        exclude=('seller','avg_score','purchaced','dikko_name','dikko_slug','tags','code','slug','created_date','modified_date')
        labels={
            'color':'رنگ',
            'size':'سایز',
            'suitble_age':'مناسب برای سن ',
            'suitble_for':'مناسب برای ',
            'type':'جنس',
            'type_sewing':'نوع دوخت',
            'height':'قد',
            'title':'نام محصول',
            'description':'توضیحات محصول',
            'price':'قیمت',
            'available_quantity':'تعداد موجود',
            'is_active':'فعال',
            'discount':'تخفیف',


        }
        widgets={
            # 'image': forms.MultiWidget(attrs={'class': 'form-control ', 'style': 'font-size: medium'}),
            'discount':forms.NumberInput(attrs={'class': 'form-control input-lg', 'style': 'font-size: large','step':0.25}),
            'color': forms.CheckboxSelectMultiple(attrs={ 'style': 'font-size: medium'}),
            'size': forms.CheckboxSelectMultiple(attrs={'style': 'font-size: medium'}),
            'suitble_age': forms.CheckboxSelectMultiple(attrs={'style': 'font-size: medium'}),
            'suitble_for': forms.CheckboxSelectMultiple(attrs={'style': 'font-size: medium'}),
            'height': forms.CheckboxSelectMultiple(attrs={'style': 'font-size: medium'}),
            'type_sewing': forms.CheckboxSelectMultiple(attrs={'style': 'font-size: medium'}),
            'type': forms.CheckboxSelectMultiple(attrs={'style': 'font-size: medium'}),
            'title': forms.TextInput(attrs={'class': 'form-control input-lg','style': 'font-size: large'}),
            'price': forms.NumberInput(attrs={'class': 'form-control input-lg', 'style': 'font-size: large'}),
            'available_quantity': forms.NumberInput(attrs={'class': 'form-control input-lg', 'style': 'font-size: large'}),
        }

    def __init__(self, *args, **kwargs):
        super(add_dress, self).__init__(*args, **kwargs)
        self.fields['discount'].required=False
        self.fields['image'].required=False


class add_zivar(forms.ModelForm):
    class Meta:
        model=Zivar
        exclude=('seller', 'avg_score', 'purchaced', 'dikko_name', 'dikko_slug', 'tags', 'code', 'slug','modified_date','created_date')
        labels={
            'color': 'رنگ',
            'weight': 'وزن',
            'suitble_age': 'مناسب برای سن ',
            'suitble_for': 'مناسب برای ',
            'type': 'جنس',
            'madeIn': 'ساخت',
            'title': 'نام محصول',
            'description': 'توضیحات محصول',
            'price': 'قیمت',
            'available_quantity': 'تعداد موجود',
            'is_active': 'فعال',
            'discount': 'تخفیف',
        }
        widgets={
            # 'image': forms.MultiWidget(attrs={'class': 'form-control ', 'style': 'font-size: medium'}),
            'discount': forms.NumberInput(
                attrs={'class': 'form-control input-lg', 'style': 'font-size: large', 'step': 0.25}),
            'color': forms.CheckboxSelectMultiple(attrs={'style': 'font-size: medium'}),
            'weight': forms.TextInput(attrs={'class': 'form-control input-lg', 'style': 'font-size: large'}),
            'suitble_age': forms.CheckboxSelectMultiple(attrs={'style': 'font-size: medium'}),
            'suitble_for': forms.CheckboxSelectMultiple(attrs={'style': 'font-size: medium'}),
            'type': forms.TextInput(attrs={'class': 'form-control input-lg', 'style': 'font-size: large'}),
            'title': forms.TextInput(attrs={'class': 'form-control input-lg', 'style': 'font-size: large'}),
            'price': forms.NumberInput(attrs={'class': 'form-control input-lg', 'style': 'font-size: large'}),
            'madeIn':forms.TextInput(attrs={'class': 'form-control input-lg', 'style': 'font-size: large'}),
            'available_quantity': forms.NumberInput(
                attrs={'class': 'form-control input-lg', 'style': 'font-size: large'}),
        }

    def __init__(self, *args, **kwargs):
            super(add_zivar, self).__init__(*args, **kwargs)
            self.fields['discount'].required=False
            self.fields['weight'].required=False
            self.fields['image'].required=False
            self.fields['suitble_for'].required=False

class add_ruDuzi(forms.ModelForm):
    class Meta:
        model=RuDuzi
        exclude=('seller', 'avg_score', 'purchaced', 'dikko_name', 'dikko_slug', 'tags', 'code', 'slug','modified_date','created_date')
        labels={
            'dimensions':'ابعاد',
            'color': 'رنگ',
            'weight': 'وزن',
            'suitble_age': 'مناسب برای سن ',
            'suitble_for': 'مناسب برای ',
            'type': 'جنس',
            'madeIn': 'ساخت',
            'title': 'نام محصول',
            'description': 'توضیحات محصول',
            'price': 'قیمت',
            'available_quantity': 'تعداد موجود',
            'is_active': 'فعال',
            'discount': 'تخفیف',
        }
        widgets={
            # 'image': forms.MultiWidget(attrs={'class': 'form-control ', 'style': 'font-size: medium'}),
            'dimensions':forms.TextInput(attrs={'class': 'form-control input-lg', 'style': 'font-size: large'}),
            'discount': forms.NumberInput(
                attrs={'class': 'form-control input-lg', 'style': 'font-size: large', 'step': 0.25}),
            'color': forms.CheckboxSelectMultiple(attrs={'style': 'font-size: medium'}),
            'weight': forms.TextInput(attrs={'class': 'form-control input-lg', 'style': 'font-size: large'}),
            'suitble_age': forms.CheckboxSelectMultiple(attrs={'style': 'font-size: medium'}),
            'suitble_for': forms.CheckboxSelectMultiple(attrs={'style': 'font-size: medium'}),
            'type': forms.TextInput(attrs={'class': 'form-control input-lg', 'style': 'font-size: large'}),
            'title': forms.TextInput(attrs={'class': 'form-control input-lg', 'style': 'font-size: large'}),
            'price': forms.NumberInput(attrs={'class': 'form-control input-lg', 'style': 'font-size: large'}),
            'madeIn':forms.TextInput(attrs={'class': 'form-control input-lg', 'style': 'font-size: large'}),
            'available_quantity': forms.NumberInput(
                attrs={'class': 'form-control input-lg', 'style': 'font-size: large'}),
        }

    def __init__(self, *args, **kwargs):
            super(add_ruDuzi, self).__init__(*args, **kwargs)
            self.fields['discount'].required=False
            self.fields['weight'].required=False
            self.fields['image'].required=False
            self.fields['suitble_for'].required=False

class add_DastSaze_Honari(forms.ModelForm):
    class Meta:
        model=Dast_Saze
        exclude=('seller', 'avg_score', 'purchaced', 'dikko_name', 'dikko_slug', 'tags', 'code', 'slug','modified_date','created_date')
        labels={
            'dimensions':'ابعاد',
            'color': 'رنگ',
            'weight': 'وزن',
            'suitble_age': 'مناسب برای سن ',
            'suitble_for': 'مناسب برای ',
            'type': 'جنس',
            'madeIn': 'ساخت',
            'title': 'نام محصول',
            'description': 'توضیحات محصول',
            'price': 'قیمت',
            'available_quantity': 'تعداد موجود',
            'is_active': 'فعال',
            'discount': 'تخفیف',
        }
        widgets={
            # 'image': forms.MultiWidget(attrs={'class': 'form-control ', 'style': 'font-size: medium'}),
            'dimensions':forms.TextInput(attrs={'class': 'form-control input-lg', 'style': 'font-size: large'}),
            'discount': forms.NumberInput(
                attrs={'class': 'form-control input-lg', 'style': 'font-size: large', 'step': 0.25}),
            'color': forms.CheckboxSelectMultiple(attrs={'style': 'font-size: medium'}),
            'weight': forms.TextInput(attrs={'class': 'form-control input-lg', 'style': 'font-size: large'}),
            'suitble_age': forms.CheckboxSelectMultiple(attrs={'style': 'font-size: medium'}),
            'suitble_for': forms.CheckboxSelectMultiple(attrs={'style': 'font-size: medium'}),
            'type': forms.TextInput(attrs={'class': 'form-control input-lg', 'style': 'font-size: large'}),
            'title': forms.TextInput(attrs={'class': 'form-control input-lg', 'style': 'font-size: large'}),
            'price': forms.NumberInput(attrs={'class': 'form-control input-lg', 'style': 'font-size: large'}),
            'madeIn':forms.TextInput(attrs={'class': 'form-control input-lg', 'style': 'font-size: large'}),
            'available_quantity': forms.NumberInput(
                attrs={'class': 'form-control input-lg', 'style': 'font-size: large'}),
        }

    def __init__(self, *args, **kwargs):
            super(add_DastSaze_Honari, self).__init__(*args, **kwargs)
            self.fields['discount'].required=False
            self.fields['weight'].required=False
            self.fields['image'].required=False
            self.fields['suitble_for'].required=False


class create_notif(forms.ModelForm):
    class Meta:
        model=Notification
        fields=('verb',)
        widgets = {
            'verb': forms.Textarea(attrs={'class': 'form-control'}),
            # 'description':forms.TextInput()
        }