from django import template
register = template.Library()

from accounts.models import Profile


@register.simple_tag
def check_seller(arg):
  	return Profile.objects.get(user=arg)

