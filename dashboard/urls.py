from django.urls import path, include, re_path
from django.contrib.auth import views as auth_views
from . import views
from discount_codes import views as discount_views
from favorite_list import views as favorite_views
from analytics import views as analytics_views
from subscribers import views as subscribers_views
from complaints import views as complain_views
app_name='dashboard'


urlpatterns=[
    path('',views.dashboard_account_information,name='dashboard_account_information'),
    path('addresses/', views.Address_List.as_view(), name='dashboard_addresses'),
    path('dikko_management/create_address',views.address_create,name='dashboard_address_create'),
    re_path('dikko_management/remove_address/(?P<pk>\d+)/$',views.Address_Delete.as_view(),name='dashboard_address_remove'),
    re_path('dikko_management/edit_address/(?P<pk>\d+)/$',views.Address_Update.as_view(),name='dashboard_address_edit'),
    re_path('dikko_management/edit_address/(?P<pk>\d+)/(?P<return>[\w-]+)$',views.Address_Update.as_view(),name='dashboard_address_edit'),
    path('dikko_management/',views.dashboard_dikko_management.as_view(),name='dashboard_dikko_management'),
    re_path(r'dikko_info/details/(?P<pk>\d+)/$',views.Dikko_Info_Details.as_view(),name='dashboard_dikko_info_details'),
    path('dikko_info/',views.Dikko_Info_List.as_view(),name='dashboard_dikko_info'),
    path('create_dikko/',views.Create_dikko.as_view(),name='dashboard_create_dikko'),
    re_path('dikko_management/remove/products_list/(?P<pk>\d+)/$',views.Dikko_Products_List.as_view(),name='dashboard_dikko_products_list_remove'),
    re_path('dikko_management/edit/products_list/(?P<pk>\d+)/$',views.Dikko_Products_List.as_view(),name='dashboard_dikko_products_list_edit'),
    path('dikko_management/pre_add',views.Dikko_Info_List.as_view(),name='dashboard_dikko_pre_add'),
    path('dikko_management/pre_remove',views.Dikko_Info_List.as_view(),name='dashboard_dikko_pre_remove'),
    path('dikko_management/pre_edit',views.Dikko_Info_List.as_view(),name='dashboard_dikko_pre_edit'),
    re_path(r'dikko_management/add_product/(?P<pk>\d+)/$',views.Dikko_Add_Product.as_view(),name='dashboard_dikko_management_add_product'),
    re_path(r'dikko_management/edit_product/(?P<pk>\d+)/(?P<pro>\d+)$',views.Dikko_Edit_Product,name='dashboard_dikko_management_edit_product'),
    re_path(r'dikko_management/remove_product/(?P<pk>\d+)/(?P<pro>\d+)$',views.Dikko_Remove_Product.as_view(),name='dashboard_dikko_management_remove_product'),
    path('ajax/load-cities/', views.load_cities, name='ajax_load_cities'),
    path('ajax/load-fields/', views.load_fields, name='ajax_load_fields'),
    path('orders/',views.dashboard_orders.as_view(),name='dashboard_orders'),
    path('notifications/create', views.create_notification, name='dashboard_notifications_create'),
    path('notifications/list', views.Notifications_List.as_view(), name='dashboard_notifications_list'),
    path('notifications/orders',views.notifications_orders_list,name='dashboard_notifications_orders'),
    path('notifications/comments',views.notifications_comments_list,name='dashboard_notifications_comments'),
    re_path(r'^notifications/orders/details/(?P<pk>\d+)/$',views.notifications_orders_details.as_view(),name='dashboard_notifications_orders_details'),
    path('dicount_codes/',discount_views.Create_Code.as_view(),name='dashboard_discount_codes'),
    path('favorite_list/',favorite_views.Favorite_List.as_view(),name='dashboard_favorite_list'),
    path('analytics/',analytics_views.home.as_view(),name='dashboard_analytics'),
    path('analytics/sell_per_product',analytics_views.sell_per_product,name='dashboard_analytics_sell_per_product'),
    path('analytics/sell_per_dikko', analytics_views.sell_per_dikko, name='dashboard_analytics_sell_per_dikko'),
    path('subscribers/', subscribers_views.communityList, name='dashboard_subscribers_communities'),
    re_path(r'^subscribers/community/(?P<slug>[\w-]+)/create_message/$', subscribers_views.createMessage, name='dashboard_subscribers_create_message'),
    re_path(r'^subscribers/community/(?P<slug>[\w-]+)/message_list/$', subscribers_views.messagesList, name='dashboard_subscribers_message_list'),
    re_path(r'^subscribers/community/(?P<slug>[\w-]+)/message_details/(?P<message>[\w-]+)$', subscribers_views.messageDetail, name='dashboard_subscribers_message_details'),
    path('complains/', complain_views.complainList, name='dashboard_complains_list'),
]
