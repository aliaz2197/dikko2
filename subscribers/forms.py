from django import forms
from .models import Message
class CreateMessageForm(forms.ModelForm):
    class Meta:
        model=Message
        fields=('message','title',)
        labels={
            'message':'پیام',
            'title':'عنوان',
        }
        widgets={
            'title':forms.TextInput(attrs={'class':'form-control', 'style': 'font-size: large'}),
            'message':forms.FileInput(attrs={'class':'form-control'}),
        }
