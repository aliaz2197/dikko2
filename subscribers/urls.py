from django.urls import path, include, re_path
from . import views

app_name="subscribers"
urlpatterns=[
    path('',views.subscribe,name='subscribe'),
]