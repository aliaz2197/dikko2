from django.contrib import admin
from .models import Subscriber,SubscribersCommunity,Message
# Register your models here.
admin.site.register(SubscribersCommunity)
admin.site.register(Subscriber)
admin.site.register(Message)