from django.contrib.sites.shortcuts import get_current_site
from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.template.loader import render_to_string
from django.utils.html import strip_tags

from .models import Subscriber,SubscribersCommunity,Message
from accounts.models import Profile
from django.contrib.auth.decorators import login_required
from .forms import CreateMessageForm
from django.core.mail import send_mail, EmailMessage
from django.conf import settings
# Create your views here.

def subscribe(request):
    if request.is_ajax():
        email=request.GET.get('email')
        community=SubscribersCommunity.objects.get(name='Email')
        try:
            subscriber=Subscriber.objects.get(email=email)
            final={'valid': 0}
            return JsonResponse(final, safe=False)
        except:
            subscriber=Subscriber(email=email,status='A',community=community)
            subscriber.save()
            final={'valid': 1}
            return JsonResponse(final, safe=False)
@login_required
def communityList(request):
    if request.user.admin:
        profile=Profile.objects.get(user=request.user)
        communities=SubscribersCommunity.objects.all()
        return render(request,'dashboard-newsFeed.html',{"profile":profile,"communities":communities})
@login_required
def createMessage(request,slug):
    if request.user.admin:
        profile=Profile.objects.get(user=request.user)
        community=SubscribersCommunity.objects.get(slug=slug)
        form=CreateMessageForm(request.POST or None,request.FILES)

        if request.POST:
            if form.is_valid():
                message=form.save(commit=False)
                message.writer=profile
                message.community=community
                message.save()
                final=message.message.path
                for subscriber in community.subscriber_set.all():
                    if subscriber.status=='A':
                        subject=message.title

                        if Profile.objects.filter(user__email=subscriber.email).exists():
                            name=Profile.objects.get(user__email=subscriber.email).user.first_name
                            html_news=render_to_string(final,{'name':name})
                        else :
                            html_news=render_to_string('accounts/account_activation_email.html')
                        plain_message=strip_tags(html_news)
                        from_email='بق بند <info@boghband.com>'
                        to=subscriber.email

                        send_mail(subject, plain_message, from_email, [to], html_message=html_news)

                return redirect('dashboard:dashboard_subscribers_communities')
            else:
                return render(request, 'dashboard-newsFeed-create-message.html', {"profile": profile,
                                                                                  "community": community, "form": form})
        return render(request, 'dashboard-newsFeed-create-message.html', {"profile": profile,
                                                                          "community":community,"form":form})
@login_required
def messagesList(request,slug):
    if request.user.admin:
        profile=Profile.objects.get(user=request.user)
        community=SubscribersCommunity.objects.get(slug=slug)
        messages=community.message_set.all()
        return render(request, 'dashboard-newsFeed-message-list.html', {"profile": profile,
                                                                          "community": community,"messages":messages})
@login_required
def messageDetail(request,slug,message):
    if request.user.admin:
        profile=Profile.objects.get(user=request.user)
        community=SubscribersCommunity.objects.get(slug=slug)
        message=Message.objects.get(slug=message)

        return render(request,'dashboard-newsFeed-message-details.html',{"profile": profile,
                                                                          "community": community,"message":message})
