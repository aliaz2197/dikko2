from django.db import models
from django.urls import reverse

from dikko2.utils import unique_slug_generator
from django.db.models.signals import pre_save
from accounts.models import Profile
from tinymce.models import HTMLField
# Create your models here.
from django.utils import timezone

STATUS= (
    ('A', 'فعال'), # Active
    ('I', 'غیر فعال'),  # inActive
)

class SubscribersCommunity(models.Model):
    name=models.CharField(max_length=20,null=True,blank=True)
    slug=models.SlugField(blank=True, unique=True, allow_unicode=True)

    def __str__(self):
        return self.name
def community_pre_receiver(sender,instance,*args,**kwargs):
    if not instance.slug:
        instance.slug=unique_slug_generator(instance)


pre_save.connect(community_pre_receiver,sender=SubscribersCommunity)


def get_upload_path(instance, filename):
    return 'newsFeed/{0}/{1}'.format(instance.title, filename)
class Message(models.Model):
    title=models.CharField(max_length=80,blank=True,null=True)
    slug=models.SlugField(blank=True, unique=True, allow_unicode=True)
    message=models.FileField(blank=True,null=True,upload_to=get_upload_path)
    date=models.DateTimeField()
    community=models.ForeignKey(SubscribersCommunity , on_delete=models.CASCADE,null=True)
    writer=models.ForeignKey(Profile,on_delete=models.SET_NULL,null=True)

    def get_absolute_url(self):
        return reverse('dashboard:dashboard_subscribers_message_details',kwargs={'message':self.slug,'slug':self.community.slug})
    def save(self, *args, **kwargs):
        if not self.id:
            self.date=timezone.localtime(timezone.now())
        return super(Message, self).save(*args, **kwargs)
    def __str__(self):
        return self.title

def message_pre_receiver(sender,instance,*args,**kwargs):
    if not instance.slug:
        instance.slug=unique_slug_generator(instance)


pre_save.connect(message_pre_receiver,sender=Message)

class Subscriber(models.Model):
        email=models.EmailField()
        status=models.CharField(max_length=1, choices=STATUS)
        community=models.ForeignKey(SubscribersCommunity, on_delete=models.CASCADE,null=True)

        def __str__(self):
            return self.email
