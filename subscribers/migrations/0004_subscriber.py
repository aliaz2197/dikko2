# Generated by Django 2.0.2 on 2019-02-05 22:55

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('subscribers', '0003_auto_20190206_0144'),
    ]

    operations = [
        migrations.CreateModel(
            name='Subscriber',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('email', models.EmailField(max_length=254)),
                ('status', models.CharField(choices=[('A', 'فعال'), ('I', 'غیر فعال')], max_length=1)),
                ('community', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='subscribers.SubscribersCommunity')),
            ],
        ),
    ]
